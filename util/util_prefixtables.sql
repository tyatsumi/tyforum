-------------------------------------------------------------------------------
--    mwForum - Web-based discussion forum
--    Copyright (c) 1999-2015 Markus Wichitill
--
--    This program is free software; you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation; either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
-------------------------------------------------------------------------------

-- Rename tables to prefixed names
ALTER TABLE arc_boards RENAME TO tyf_arc_boards;
ALTER TABLE arc_topics RENAME TO tyf_arc_topics;
ALTER TABLE arc_posts RENAME TO tyf_arc_posts;
ALTER TABLE attachments RENAME TO tyf_attachments;
ALTER TABLE boardAdminGroups RENAME TO tyf_boardAdminGroups;
ALTER TABLE boardHiddenFlags RENAME TO tyf_boardHiddenFlags;
ALTER TABLE boardMemberGroups RENAME TO tyf_boardMemberGroups;
ALTER TABLE boards RENAME TO tyf_boards;
ALTER TABLE boardSubscriptions RENAME TO tyf_boardSubscriptions;
ALTER TABLE categories RENAME TO tyf_categories;
ALTER TABLE chat RENAME TO tyf_chat;
ALTER TABLE config RENAME TO tyf_config;
ALTER TABLE groupAdmins RENAME TO tyf_groupAdmins;
ALTER TABLE groupMembers RENAME TO tyf_groupMembers;
ALTER TABLE groups RENAME TO tyf_groups;
ALTER TABLE log RENAME TO tyf_log;
ALTER TABLE messages RENAME TO tyf_messages;
ALTER TABLE notes RENAME TO tyf_notes;
ALTER TABLE pollOptions RENAME TO tyf_pollOptions;
ALTER TABLE polls RENAME TO tyf_polls;
ALTER TABLE pollVotes RENAME TO tyf_pollVotes;
ALTER TABLE postLikes RENAME TO tyf_postLikes;
ALTER TABLE postReports RENAME TO tyf_postReports;
ALTER TABLE posts RENAME TO tyf_posts;
ALTER TABLE tickets RENAME TO tyf_tickets;
ALTER TABLE topicReadTimes RENAME TO tyf_topicReadTimes;
ALTER TABLE topics RENAME TO tyf_topics;
ALTER TABLE topicSubscriptions RENAME TO tyf_topicSubscriptions;
ALTER TABLE userBadges RENAME TO tyf_userBadges;
ALTER TABLE userBans RENAME TO tyf_userBans;
ALTER TABLE userIgnores RENAME TO tyf_userIgnores;
ALTER TABLE users RENAME TO tyf_users;
ALTER TABLE userVariables RENAME TO tyf_userVariables;
ALTER TABLE variables RENAME TO tyf_variables;
ALTER TABLE watchUsers RENAME TO tyf_watchUsers;
ALTER TABLE watchWords RENAME TO tyf_watchWords;

-- Rename tables to unprefixed names
ALTER TABLE tyf_arc_boards RENAME TO arc_boards;
ALTER TABLE tyf_arc_topics RENAME TO arc_topics;
ALTER TABLE tyf_arc_posts RENAME TO arc_posts;
ALTER TABLE tyf_attachments RENAME TO attachments;
ALTER TABLE tyf_boardAdminGroups RENAME TO boardAdminGroups;
ALTER TABLE tyf_boardHiddenFlags RENAME TO boardHiddenFlags;
ALTER TABLE tyf_boardMemberGroups RENAME TO boardMemberGroups;
ALTER TABLE tyf_boards RENAME TO boards;
ALTER TABLE tyf_boardSubscriptions RENAME TO boardSubscriptions;
ALTER TABLE tyf_categories RENAME TO categories;
ALTER TABLE tyf_chat RENAME TO chat;
ALTER TABLE tyf_config RENAME TO config;
ALTER TABLE tyf_groupAdmins RENAME TO groupAdmins;
ALTER TABLE tyf_groupMembers RENAME TO groupMembers;
ALTER TABLE tyf_groups RENAME TO groups;
ALTER TABLE tyf_log RENAME TO log;
ALTER TABLE tyf_messages RENAME TO messages;
ALTER TABLE tyf_notes RENAME TO notes;
ALTER TABLE tyf_pollOptions RENAME TO pollOptions;
ALTER TABLE tyf_polls RENAME TO polls;
ALTER TABLE tyf_pollVotes RENAME TO pollVotes;
ALTER TABLE tyf_postLikes RENAME TO postLikes;
ALTER TABLE tyf_postReports RENAME TO postReports;
ALTER TABLE tyf_posts RENAME TO posts;
ALTER TABLE tyf_tickets RENAME TO tickets;
ALTER TABLE tyf_topicReadTimes RENAME TO topicReadTimes;
ALTER TABLE tyf_topics RENAME TO topics;
ALTER TABLE tyf_topicSubscriptions RENAME TO topicSubscriptions;
ALTER TABLE tyf_userBadges RENAME TO userBadges;
ALTER TABLE tyf_userBans RENAME TO userBans;
ALTER TABLE tyf_userIgnores RENAME TO userIgnores;
ALTER TABLE tyf_users RENAME TO users;
ALTER TABLE tyf_userVariables RENAME TO userVariables;
ALTER TABLE tyf_variables RENAME TO variables;
ALTER TABLE tyf_watchUsers RENAME TO watchUsers;
ALTER TABLE tyf_watchWords RENAME TO watchWords;

-- Move tables to PgSQL schema
ALTER TABLE arc_boards SET SCHEMA tyf;
ALTER TABLE arc_topics SET SCHEMA tyf;
ALTER TABLE arc_posts SET SCHEMA tyf;
ALTER TABLE attachments SET SCHEMA tyf;
ALTER TABLE boardAdminGroups SET SCHEMA tyf;
ALTER TABLE boardHiddenFlags SET SCHEMA tyf;
ALTER TABLE boardMemberGroups SET SCHEMA tyf;
ALTER TABLE boards SET SCHEMA tyf;
ALTER TABLE boardSubscriptions SET SCHEMA tyf;
ALTER TABLE categories SET SCHEMA tyf;
ALTER TABLE chat SET SCHEMA tyf;
ALTER TABLE config SET SCHEMA tyf;
ALTER TABLE groupAdmins SET SCHEMA tyf;
ALTER TABLE groupMembers SET SCHEMA tyf;
ALTER TABLE groups SET SCHEMA tyf;
ALTER TABLE log SET SCHEMA tyf;
ALTER TABLE messages SET SCHEMA tyf;
ALTER TABLE notes SET SCHEMA tyf;
ALTER TABLE pollOptions SET SCHEMA tyf;
ALTER TABLE polls SET SCHEMA tyf;
ALTER TABLE pollVotes SET SCHEMA tyf;
ALTER TABLE postLikes SET SCHEMA tyf;
ALTER TABLE postReports SET SCHEMA tyf;
ALTER TABLE posts SET SCHEMA tyf;
ALTER TABLE tickets SET SCHEMA tyf;
ALTER TABLE topicReadTimes SET SCHEMA tyf;
ALTER TABLE topics SET SCHEMA tyf;
ALTER TABLE topicSubscriptions SET SCHEMA tyf;
ALTER TABLE userBadges SET SCHEMA tyf;
ALTER TABLE userBans SET SCHEMA tyf;
ALTER TABLE userIgnores SET SCHEMA tyf;
ALTER TABLE users SET SCHEMA tyf;
ALTER TABLE userVariables SET SCHEMA tyf;
ALTER TABLE variables SET SCHEMA tyf;
ALTER TABLE watchUsers SET SCHEMA tyf;
ALTER TABLE watchWords SET SCHEMA tyf;
