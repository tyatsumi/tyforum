#------------------------------------------------------------------------------
#    mwForum - Web-based discussion forum
#    Copyright © 1999-2014 Markus Wichitill
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#------------------------------------------------------------------------------

package TyfJapanese;
use utf8;
use strict;
our $VERSION = "2.29.3";
our $lng     = {};

#------------------------------------------------------------------------------

# Default to English for missing strings
require TyfEnglish;
%$lng = %$TyfEnglish::lng;

#------------------------------------------------------------------------------

# Language module meta information
$lng->{author} = "Aki Goto";

#------------------------------------------------------------------------------

# Common strings
$lng->{comUp}        = "上";                    #"Up";
$lng->{comUpTT}      = "1段上に行く";               #"Go up a level";
$lng->{comPgPrev}    = "前";                    #"Previous";
$lng->{comPgPrevTT}  = "前のページに行く";             #"Go to previous page";
$lng->{comPgNext}    = "次";                    #"Next";
$lng->{comPgNextTT}  = "次のページに行く";             #"Go to next page";
$lng->{comBoardList} = "フォーラム";                #"Forum";
$lng->{comNew}       = "N";                    #"N";
$lng->{comNewTT}     = "New";                  #"New";
$lng->{comOld}       = "-";                    #"-";
$lng->{comOldTT}     = "古い";                   #"Old";
$lng->{comNewUnrd}   = "N/U";                  #"N/U";
$lng->{comNewUnrdTT} = "新/未読";                 #"New/Unread";
$lng->{comNewRead}   = "N/-";                  #"N/-";
$lng->{comNewReadTT} = "新/既読";                 #"New/Read";
$lng->{comOldUnrd}   = "-/U";                  #"-/U";
$lng->{comOldUnrdTT} = "古/未読";                 #"Old/Unread";
$lng->{comOldRead}   = "-/-";                  #"-/-";
$lng->{comOldReadTT} = "古/既読";                 #"Old/Read";
$lng->{comAnswer}    = "A";                    #"A";
$lng->{comAnswerTT}  = "回答済み";                 #"Answered";
$lng->{comShowNew}   = "新しい投稿";                #"New Posts";
$lng->{comShowNewTT} = "新しい投稿を表示する";           #"Show new posts";
$lng->{comShowUnr}   = "未読の投稿";                #"Unread Posts";
$lng->{comShowUnrTT} = "未読の投稿を表示する";           #"Show unread posts";
$lng->{comFeeds}     = "フィード";                 #"Feeds";
$lng->{comFeedsTT}   = "Atom/RSSフィードを表示する";    #"Show Atom/RSS feeds";
$lng->{comCaptcha}   = "対スパム画像の6文字を入力してください"
  ;    #"Please type the six characters from the anti-spam image";

# Header
$lng->{hdrForum}     = "フォーラム";              #"Forum";
$lng->{hdrForumTT}   = "フォーラムスタートページ";       #"Forum start page";
$lng->{hdrHomeTT}    = "関連ホームページ";           #"Associated homepage";
$lng->{hdrProfile}   = "プロファイル";             #"Profile";
$lng->{hdrProfileTT} = "ユーザプロファイルを編集";       #"Edit user profile";
$lng->{hdrOptions}   = "オプション";              #"Options";
$lng->{hdrOptionsTT} = "ユーザオプションを編集";        #"Edit user options";
$lng->{hdrHelp}      = "ヘルプ";                #"Help";
$lng->{hdrHelpTT}    = "ヘルプとFAQ";            #"Help and FAQ";
$lng->{hdrSearch}    = "検索";                 #"Search";
$lng->{hdrSearchTT}  = "投稿をキーワードで検索する";      #"Search posts for keywords";
$lng->{hdrChat}      = "チャット";               #"Chat";
$lng->{hdrChatTT}    = "チャットメッセージを読み書きする";   #"Read and write chat messages";
$lng->{hdrMsgs}      = "メッセージ";              #"Messages";
$lng->{hdrMsgsTT}    = "プライベートメッセージを読み書きする"; #"Read and write private messages";
$lng->{hdrLogin}     = "ログイン";               #"Login";
$lng->{hdrLoginTT}   = "ユーザ名とパスワードでログインする"; #"Login with username and password";
$lng->{hdrLogout}    = "ログアウト";             #"Logout";
$lng->{hdrLogoutTT}  = "ログアウトする";           #"Logout";
$lng->{hdrReg}       = "登録";                #"Register";
$lng->{hdrRegTT}     = "ユーザアカウントを登録する";     #"Register user account";
$lng->{hdrOpenId}    = "OpenID";            #"OpenID";
$lng->{hdrOpenIdTT}  = "OpenIDでログインする";     #"Login with OpenID";
$lng->{hdrNoLogin}   = "未ログイン";             #"Not logged in";
$lng->{hdrWelcome}   = "ログイン済み";            #"Logged in as";
$lng->{hdrArchive}   = "アーカイブ";             #"Archive";

# Forum page
$lng->{frmTitle}     = "フォーラム";           #"Forum";
$lng->{frmMarkOld}   = "[古い]にする";         #"Mark Old";
$lng->{frmMarkOldTT} = "すべての投稿を[古い]にする";  #"Mark all posts as old";
$lng->{frmMarkRd}    = "既読にする";           #"Mark Read";
$lng->{frmMarkRdTT}  = "すべての投稿を既読にする";    #"Mark all posts as read";
$lng->{frmUsers}     = "ユーザ";             #"Users";
$lng->{frmUsersTT}   = "ユーザリストを表示する";     #"Show user list";
$lng->{frmAttach}    = "添付";              #"Attachments";
$lng->{frmAttachTT}  = "添付リストを表示する";      #"Show attachment list";
$lng->{frmInfo}      = "情報";              #"Info";
$lng->{frmInfoTT}    = "フォーラム情報を表示する";    #"Show forum info";
$lng->{frmNotTtl}    = "通知";              #"Notifications";
$lng->{frmNotDelB}   = "通知を除去";           #"Remove notifications";
$lng->{frmCtgCollap} = "カテゴリをたたむ";        #"Collapse category";
$lng->{frmCtgExpand} = "カテゴリを展開する";       #"Expand category";
$lng->{frmPosts}     = "投稿数";             #"Posts";
$lng->{frmLastPost}  = "最新の投稿";           #"Last Post";
$lng->{frmRegOnly}   = "登録ユーザ限定";         #"Registered users only";
$lng->{frmMbrOnly}   = "掲示板メンバ限定";        #"Board members only";
$lng->{frmNew}       = "new";             #"new";
$lng->{frmNoBoards}  = "見える掲示板はありません。";   #"No visible boards.";
$lng->{frmStats}     = "統計";              #"Statistics";
$lng->{frmOnlUsr}    = "オンライン";           #"Online";
$lng->{frmOnlUsrTT}  = "過去5分間にオンラインのユーザ"; #"Users online in the past 5 minutes";
$lng->{frmNewUsr}    = "新しいユーザ";          #"New";
$lng->{frmNewUsrTT}  = "過去5日間に登録したユーザ"; #"Users registered in the past 5 days";
$lng->{frmBdayUsr}   = "誕生日";           #"Birthday";
$lng->{frmBdayUsrTT} = "今日が誕生日のユーザ";    #"Users that have their birthday today";

# Forum info page
$lng->{fifTitle}     = "フォーラム";         #"Forum";
$lng->{fifBrowsers}  = "ブラウザ";          #"Browsers";
$lng->{fifCountries} = "国";             #"Countries";
$lng->{fifActivity}  = "活動";            #"Activity";
$lng->{fifGenTtl}    = "一般情報";          #"General Info";
$lng->{fifGenAdmEml} = "メールアドレス";       #"Email Address";
$lng->{fifGenAdmins} = "管理者";           #"Administrators";
$lng->{fifGenTZone}  = "タイムゾーン";        #"Timezone";
$lng->{fifGenVer}    = "フォーラムバージョン";    #"Forum Version";
$lng->{fifGenLang}   = "言語";            #"Languages";
$lng->{fifStsTtl}    = "統計";            #"Statistics";
$lng->{fifStsUsrNum} = "ユーザ";           #"Users";
$lng->{fifStsTpcNum} = "トピック";          #"Topics";
$lng->{fifStsPstNum} = "投稿";            #"Posts";

# User agents page
$lng->{uasTitle}  = "ユーザエージェント";                              #"User Agents";
$lng->{uasUsersT} = "過去[[days]]日間で[[users]]ユーザのログインがありました。"
  ;    #"Counting [[users]] users logged in during the last [[days]] days.";
$lng->{uasChartTtl} = "チャート";            #"Charts";
$lng->{uasUaTtl}    = "ブラウザ";            #"Browsers";
$lng->{uasOsTtl}    = "オペレーティングシステム";    #"Operating Systems";

# User countries page
$lng->{ucoTitle}    = "ユーザの国";           #"User Countries";
$lng->{ucoMapTtl}   = "地図";              #"Map";
$lng->{ucoCntryTtl} = "国";               #"Countries";

# Forum activity page
$lng->{actTitle}   = "フォーラム活動";          #"Forum Activity";
$lng->{actPstDayT} = "横軸: 1日あたり1ピクセル、縦軸: 1投稿あたり1ピクセル。現存する投稿だけ計数されています。"
  ; #"Horizontal axis: one pixel per day, vertical axis: one pixel per post. Only existing posts are counted.";
$lng->{actPstDayTtl} = "1日あたりの投稿数";    #"Posts Per Day";
$lng->{actPstYrTtl}  = "1年あたりの投稿数";    #"Posts Per Year";

# User activity page
$lng->{uacTitle}   = "ユーザ";                                             #"User";
$lng->{uacPstDayT} = "横軸: 1日あたり1ピクセル、縦軸: 1投稿あたり3ピクセル。現存する投稿だけ計数されています。"
  ; #"Horizontal axis: one pixel per day, vertical axis: three pixels per post. Only existing posts are counted.";

# New/unread overview page
$lng->{ovwTitleNew}  = "新しい投稿";             #"New Posts";
$lng->{ovwTitleUnr}  = "未読の投稿";             #"Unread Posts";
$lng->{ovwMore}      = "もっと";               #"More";
$lng->{ovwMoreTT}    = "次のページにもっと投稿を表示する";  #"Show more posts on the next page";
$lng->{ovwRefresh}   = "更新";                #"Refresh";
$lng->{ovwRefreshTT} = "ページを更新する";          #"Refresh page";
$lng->{ovwMarkOld}   = "[古い]にする";           #"Mark Old";
$lng->{ovwMarkOldTT} = "すべての投稿を[古い]にする";    #"Mark all posts as old";
$lng->{ovwMarkRd}    = "既読にする";             #"Mark Read";
$lng->{ovwMarkRdTT}  = "すべての投稿を既読にする";      #"Mark all posts as read";
$lng->{ovwFltTpc}    = "フィルタ";              #"Filter";
$lng->{ovwFltTpcTT}  = "このトピックだけ表示する";      #"Only show this topic";
$lng->{ovwEmpty}     = "見える投稿はありません。";      #"No visible posts found.";
$lng->{ovwMaxCutoff} = "トピックにあまりにも多くの投稿があるので、残りを飛ばしています。"
  ;    #"Topic has too many posts, skipping rest.";

# Board page
$lng->{brdTitle}    = "掲示板";                 #"Board";
$lng->{brdNewTpc}   = "投稿";                  #"Post";
$lng->{brdNewTpcTT} = "新しいトピックを投稿する";        #"Post new topic";
$lng->{brdInfo}     = "情報";                  #"Info";
$lng->{brdInfoTT}   = "掲示板の情報を表示する";         #"Show board info";
$lng->{brdMarkRd}   = "既読にする";               #"Mark Read";
$lng->{brdMarkRdTT} = "掲示板のすべての投稿を既読にする";    #"Mark all posts in board as read";
$lng->{brdTopic}    = "トピック";                #"Topic";
$lng->{brdPoster}   = "ユーザ";                 #"User";
$lng->{brdPosts}    = "投稿";                  #"Posts";
$lng->{brdLastPost} = "最新の投稿";               #"Last Post";
$lng->{brdLocked}   = "凍";                   #"L";
$lng->{brdLockedTT} = "凍結されている";             #"Locked";
$lng->{brdInvis}    = "隠";                   #"I";
$lng->{brdInvisTT}  = "見えない";                #"Invisible";
$lng->{brdPoll}     = "票";                   #"P";
$lng->{brdPollTT}   = "投票";                  #"Poll";
$lng->{brdNew}      = "new";                 #"new";
$lng->{brdAdmin}    = "管理";                  #"Administration";
$lng->{brdAdmRep}   = "報告";                  #"Reports";
$lng->{brdAdmRepTT} = "報告された投稿を表示する";        #"Show reported posts";
$lng->{brdAdmGrp}   = "グループ";                #"Groups";
$lng->{brdAdmGrpTT} = "グループ権限を編集する";         #"Edit group permissions";
$lng->{brdAdmSpl}   = "分割";                  #"Split";
$lng->{brdAdmSplTT} =
  "複数のトピックを他の掲示板に一度に移動する";    #"Mass-move topics to other boards";
$lng->{brdBoardFeed} = "掲示板フィード";    #"Board Feed";

# Board info page
$lng->{bifTitle}  = "掲示板";           #"Board";
$lng->{bifOptTtl} = "オプション";
"Options";
$lng->{bifOptDesc}  = "説明";                       #"Description";
$lng->{bifOptLock}  = "凍結";                       #"Locking";
$lng->{bifOptLockT} = "日後(最新の投稿から)、トピックは凍結されます"
  ;    #"days after last post, topics will be locked"; # XXX parameter required
$lng->{bifOptExp}  = "期限切れ";                     #"Expiration";
$lng->{bifOptExpT} = "日後(最新の投稿から)、トピックは削除されます"
  ;    #"days after last post, topics will be deleted"; # XXX parameter required
$lng->{bifOptAttc}  = "添付";              #"Attachments";
$lng->{bifOptAttcY} = "ファイル添付ができます";     #"File attachments are enabled";
$lng->{bifOptAttcN} = "ファイル添付はできません";    #"File attachments are disabled";
$lng->{bifOptAprv}  = "モデレーション";         #"Moderation";
$lng->{bifOptAprvY} =
  "投稿が見えるようになるには承認される必要があります";    #"Posts have to be approved to be visible";
$lng->{bifOptAprvN} = "投稿が見えるようになるために承認される必要はありません"
  ;    #"Posts don't have to be approved to be visible";
$lng->{bifOptPriv}  = "閲覧権限";                        #"Read Access";
$lng->{bifOptPriv0} = "すべてのユーザが掲示板を見ることができます";       #"All users can see board";
$lng->{bifOptPriv1} = "管理者/モデレータ/メンバだけが掲示板を見ることができます"
  ;    #"Only admins/moderators/members can see board";
$lng->{bifOptPriv2} =
  "登録ユーザだけが掲示板を見ることができます";    #"Only registered users can see board";
$lng->{bifOptAnnc}  = "書き込み権限";            #"Write Access";
$lng->{bifOptAnnc0} = "すべてのユーザが投稿できます";    #"All users can post";
$lng->{bifOptAnnc1} =
  "管理者/モデレータ/メンバだけが投稿できます";    #"Only admins/moderators/members can post";
$lng->{bifOptAnnc2} = "管理者/モデレータ/メンバだけがトピックを開始でき、すべてのユーザが返信できます"
  ;    #"Only admins/moderators/members can start topics, all users can reply";
$lng->{bifOptUnrg} = "登録";    #"Registration";
$lng->{bifOptUnrgY} =
  "投稿するために登録は必要ありません";        #"Posting doesn't require registration";
$lng->{bifOptUnrgN} = "投稿するために登録が必要です";    #"Posting requires registration";
$lng->{bifOptFlat}  = "スレッド型";             #"Threading";
$lng->{bifOptFlatY} = "トピックは非スレッド型です";     #"Topics are non-threaded";
$lng->{bifOptFlatN} = "トピックはスレッド型です";      #"Topics are threaded";
$lng->{bifAdmsTtl}  = "モデレータグループ";         #"Moderator Groups";
$lng->{bifMbrsTtl}  = "メンバグループ";           #"Member Groups";
$lng->{bifStatTtl}  = "統計";                #"Statistics";
$lng->{bifStatTPst} = "投稿";                #"Posts";
$lng->{bifStatLPst} = "最新の投稿";             #"Last Post";

# Topic page
$lng->{tpcTitle}   = "トピック";               #"Topic";
$lng->{tpcTpcRepl} = "投稿";                 #"Post";
$lng->{tpcTpcReplTT} =
  "トピック全般に関する投稿をする";    #"Post concerning the topic in general";
$lng->{tpcTag}       = "タグ";              #"Tag";
$lng->{tpcTagTT}     = "トピックタグを付ける";      #"Set topic tag";
$lng->{tpcSubs}      = "購読登録";            #"Subscribe";
$lng->{tpcSubsTT}    = "トピックのメール購読を登録する"; #"Enable email subscription of topic";
$lng->{tpcPolAdd}    = "投票";              #"Poll";
$lng->{tpcPolAddTT}  = "投票を追加する";         #"Add poll";
$lng->{tpcPolDel}    = "削除";              #"Delete";
$lng->{tpcPolDelTT}  = "投票を削除する";         #"Delete poll";
$lng->{tpcPolLock}   = "締切";              #"Close";
$lng->{tpcPolLockTT} = "投票を締め切る(元に戻せない)"; #"Close poll (irreversible)";
$lng->{tpcPolTtl}    = "投票";              #"Poll";
$lng->{tpcPolLocked} = "(終了)";            #"(Closed)";
$lng->{tpcPolVote}   = "投票する";            #"Vote";
$lng->{tpcPolShwRes} = "結果を表示する";         #"Show results";
$lng->{tpcHidTtl}    = "隠された投稿";          #"Hidden post";
$lng->{tpcHidIgnore} = "(無視されている)";       #"(ignored) ";
$lng->{tpcHidUnappr} = "(未承認)";           #"(unapproved) ";
$lng->{tpcLike}      = "いいね";             #"Upvote";
$lng->{tpcLikeTT}    = "投稿に[いいね]を付ける";    #"Mark post as liked";
$lng->{tpcUnlike}    = "[いいね]を消す";        #"Unvote";
$lng->{tpcUnlikeTT}  = "[いいね]を消す";        #"Revoke previous vote";
$lng->{tpcApprv}     = "承認";              #"Approve";
$lng->{tpcApprvTT}   = "投稿をユーザに見えるようにする"; #"Make post visible to users";
$lng->{tpcLock}      = "凍結";              #"Lock";
$lng->{tpcLockTT} =
  "投稿を編集と返信ができないよう凍結する";    #"Lock post to disable editing and replying";
$lng->{tpcUnlock} = "凍結解除"; #"Unlock";
$lng->{tpcUnlockTT} =
  "投稿を編集と返信ができるよう凍結解除する";    #"Unlock post to enable editing and replying";
$lng->{tpcReport} = "通知";    #"Notify";
$lng->{tpcReportTT} =
  "この投稿についてユーザやモデレータに通知する";    #"Notify users or moderators about this post";
$lng->{tpcBranch}    = "枝";                  #"Branch";
$lng->{tpcBranchTT}  = "枝を昇格/移動/凍結/削除する";    #"Promote/move/lock/delete branch";
$lng->{tpcEdit}      = "編集";                 #"Edit";
$lng->{tpcEditTT}    = "投稿を編集する";            #"Edit post";
$lng->{tpcDelete}    = "削除";                 #"Delete";
$lng->{tpcDeleteTT}  = "投稿を削除する";            #"Delete post";
$lng->{tpcAttach}    = "添付";                 #"Attach";
$lng->{tpcAttachTT}  = "添付をアップロードまたは削除する";   #"Upload and delete attachments";
$lng->{tpcReply}     = "返信";                 #"Reply";
$lng->{tpcReplyTT}   = "投稿に返信する";            #"Reply to post";
$lng->{tpcQuote}     = "引用";                 #"Quote";
$lng->{tpcQuoteTT}   = "引用付きで投稿に返信する";       #"Reply to post with quote";
$lng->{tpcBrnCollap} = "枝をたたむ";              #"Collapse branch";
$lng->{tpcBrnExpand} = "枝を展開";               #"Expand branch";
$lng->{tpcNxtPst}    = "次";                  #"Next";
$lng->{tpcNxtPstTT}  = "次の新しいまたは未読の投稿に行く";   #"Go to next new or unread post";
$lng->{tpcParent}    = "親";                  #"Parent";
$lng->{tpcParentTT}  = "親投稿に行く";             #"Go to parent post";
$lng->{tpcInvis}     = "I";                  #"I";
$lng->{tpcInvisTT}   = "見えない";               #"Invisible";
$lng->{tpcAttText}   = "添付";                 #"Attachment:";
$lng->{tpcAdmStik}   = "固定";                 #"Stick";
$lng->{tpcAdmUnstik} = "固定解除";               #"Unstick";
$lng->{tpcAdmLock}   = "凍結";                 #"Lock";
$lng->{tpcAdmUnlock} = "凍結解除";               #"Unlock";
$lng->{tpcAdmMove}   = "移動";                 #"Move";
$lng->{tpcAdmMerge}  = "統合";                 #"Merge";
$lng->{tpcAdmDelete} = "削除";                 #"Delete";
$lng->{tpcBy}        = "投稿者";                #"By";
$lng->{tpcOn}        = "日時";                 #"Date";
$lng->{tpcEdited}    = "編集された";              #"Edited";
$lng->{tpcLikes}     = "いいね";                #"Upvotes";
$lng->{tpcLocked}    = "(凍結中)";              #"(locked)";

# Topic subscription page
$lng->{tsbTitle}  = "トピック";                  #"Topic";
$lng->{tsbSubTtl} = "トピックの購読を登録";            #"Subscribe to Topic";
$lng->{tsbSubT2} =
  "即時購読は選択されたトピックの新しい投稿をメールであなたに即座に送ります。要約購読は収集された投稿を定期的(通常1日ごと)に送ります。"
  ; #"Instant subscriptions send out new posts in the selected topic to you by email instantly. Digest subscriptions send out collected posts regularly (usually daily).";
$lng->{tsbInstant}  = "即時購読";            #"Instant subscription";
$lng->{tsbDigest}   = "要約購読";            #"Digest subscription";
$lng->{tsbSubB}     = "購読登録";            #"Subscribe";
$lng->{tsbUnsubTtl} = "トピックの購読登録を解除";    #"Unsubscribe Topic";
$lng->{tsbUnsubB}   = "購読登録解除";          #"Unsubscribe";

# Add poll page
$lng->{aplTitle}     = "投票を追加";           #"Add Poll";
$lng->{aplPollTitle} = "投票のタイトルまたは質問";    #"Poll title or question";
$lng->{aplPollOpts}  = "選択肢(1行にひとつの選択肢、最大20の選択肢、ひとつの選択肢あたり60文字、マークアップなし)"
  ; #"Options (one option per line, max. 20 options, max. 60 characters per option, no markup)";
$lng->{aplPollMulti} =
  "異なる選択肢に複数の投票を許す";    #"Allow multiple votes for different options";
$lng->{aplPollNote} =
  "投票は編集できません。また一度誰かが投票すると投票は削除できません。なので投票を追加する前に投票タイトルと選択肢をよく確認してください。"
  ; #"You can't edit polls, and you can't delete them once someone has voted, so please check your poll title and options before adding the poll.";
$lng->{aplPollAddB} = "追加";    #"Add";

# Add report page
$lng->{arpTitle}  = "投稿";                                   #"Post";
$lng->{arpPngTtl} = "ユーザに通知";                               #"Notify User";
$lng->{arpPngT}   = "この投稿に関する通知を誰かの通知リストに送る(場合によってはメールで)"
  ; #"Sends a notification about this post to someone's notification list and optionally by email.";
$lng->{arpPngUser}   = "受取人";        #"Recipient";
$lng->{arpPngEmail}  = "メールでも送る";    #"Send by email, too";
$lng->{arpPngB}      = "通知";         #"Notify";
$lng->{arpPngMlSbPf} = "通知フォーム";     #"Notification from";
$lng->{arpPngMlT}    = "これはフォーラムソフトウェアからの投稿通知です。\nこのメールには返信しないでください。"
  ; #"This is a post notification from the forum software.\nPlease don't reply to this email.";
$lng->{arpRepTtl} = "モデレータに報告";    #"Report to Moderators";
$lng->{arpRepT}   = "もしある投稿が法律やフォーラムの規則に違反していると思ったら、あなたはそれをモデレータと管理者に報告できます。"
  ; #"If you think that a post violates the law or the rules of this forum, you can report it to moderators and administrators.";
$lng->{arpRepYarly} =
  "私は投稿を報告したい、返信するのではなくて";    #"I want to report the post, not reply to it";
$lng->{arpRepReason} = "理由";                 #"Reason";
$lng->{arpRepB}      = "報告";                 #"Report";
$lng->{arpThrTtl}    = "スレッド構造についてアドバイスする";  #"Advise about Threaded Structure";
$lng->{arpThrT} =
"あるユーザが間違った投稿に返信をしたとき、あなたはそのユーザに「トピックのスレッド構造を保つために正しい投稿に返信してほしい」という通知を送ることができます。これは同じことを投稿として公開するよりも一般に好ましいことです。管理者/モデレータによってのみ使うことができ、送り過ぎを避けるため24時間以内に受取人1人に対し1回だけ使えます。"
  ; #"If a user has posted a reply to the wrong post, you can send them a notification that asks them to reply to the correct posts to preserve the threaded structure of topics. This is generally preferable to public posts doing the same. Can only be used by admins/mods, within 24 hours and once per recipient to avoid flooding.";
$lng->{arpThrB} = "アドバイスする";    #"Advise";

# Report list page
$lng->{repTitle}   = "報告された投稿";           #"Reported Posts";
$lng->{repBy}      = "報告者";               #"Report By";
$lng->{repTopic}   = "トピック";              #"Topic";
$lng->{repPoster}  = "ユーザ";               #"User";
$lng->{repPosted}  = "投稿された";             #"Posted"; # XXX not used
$lng->{repDeleteB} = "報告を除去";             #"Remove report";
$lng->{repEmpty}   = "報告された投稿はありません。";    #"No reported posts.";

# Tag button bar
$lng->{tbbMod}      = "Mod";              #"Mod";
$lng->{tbbBold}     = "太字";               #"Bold";
$lng->{tbbItalic}   = "斜体";               #"Italic";
$lng->{tbbTeletype} = "等幅";               #"Teletype";
$lng->{tbbImage}    = "画像";               #"Image";
$lng->{tbbVideo}    = "動画";               #"Video";
$lng->{tbbCustom}   = "カスタム";             #"Custom";
$lng->{tbbInsSnip}  = "テキストを挿入";          #"Insert text";

# Reply page
$lng->{rplTitle}    = "トピック";           #"Topic";
$lng->{rplTopicTtl} = "トピック全般に関する投稿";   #"Post Concerning the Topic in General";
$lng->{rplReplyTtl} = "返信を投稿";          #"Post Reply";
$lng->{rplReplyT} =
"この掲示板はスレッド型です(つまり木構造)。単にでたらめなボタンではなく、あなたが参照している特定の投稿の返信ボタンを使ってください。トピック全般に返信したいときは、ページの上端と下端辺りにある投稿ボタンを使ってください。"
  ; #"This board is threaded (i.e. has a tree structure). Please use the Reply button of the specific post you are referring to, not just any random button. If you want to reply to the topic in general, use the Post button near the top and bottom of the page.";
$lng->{rplReplyName} = "名前";                  #"Name";
$lng->{rplReplyIRaw} = "生テキストを挿入";            #"Insert raw text";
$lng->{rplReplyRaw}  = "生テキスト(例えばソースコード)";    #"Raw text (e.g. source code)";
$lng->{rplReplyResp} = "以下への返信:";    #"In Response to"; # XXX parameter required
$lng->{rplReplyB}    = "投稿";         #"Post";
$lng->{rplReplyPrvB} = "プレビュー";      #"Preview";
$lng->{rplPrvTtl}    = "プレビュー";      #"Preview";
$lng->{rplEmailSbPf} = "次の人からの返信:";  #"Reply from"; # XXX parameter required
$lng->{rplEmailT2}   = "これはフォーラムソフトウェアからの返信通知です。\nこのメールには返信しないでください。"
  ; #"This is a reply notification from the forum software.\nPlease don't reply to this email.";
$lng->{rplAgeOrly} =
  "あなたが返信しようとしている投稿は書かれてからすでに[[age]]日たっています。こんなに古い投稿に本当に返信しますか？"
  ; #"The post you are replying to is already [[age]] days old. Are you sure that you want to reply to a post that old?";
$lng->{rplAgeYarly} =
  "はい、私にはそうする十分な理由があります";    #"Yes, I have a good reason for doing so";

# New topic page
$lng->{ntpTitle}   = "掲示板";                 #"Board";
$lng->{ntpTpcTtl}  = "新しいトピックを投稿";          #"Post New Topic";
$lng->{ntpTpcName} = "名前";                  #"Name";
$lng->{ntpTpcSbj}  = "題名";                  #"Subject";
$lng->{ntpTpcIRaw} = "生テキストを挿入";            #"Insert raw text";
$lng->{ntpTpcRaw}  = "生テキスト(例えばソースコード)";    #"Raw text (e.g. source code)";
$lng->{ntpTpcNtfy} = "返信通知を受け取る";           #"Receive reply notifications";
$lng->{ntpTpcB}    = "投稿";                  #"Post";
$lng->{ntpTpcPrvB} = "プレビュー";               #"Preview";
$lng->{ntpPrvTtl}  = "プレビュー";               #"Preview";

# Post edit page
$lng->{eptTitle}    = "投稿";                  #"Post";
$lng->{eptEditTtl}  = "投稿を編集";               #"Edit Post";
$lng->{eptEditSbj}  = "題名";                  #"Subject";
$lng->{eptEditIRaw} = "生テキストを挿入";            #"Insert raw text";
$lng->{eptEditRaw}  = "生テキスト(例えばソースコード)";    #"Raw text (e.g. source code)";
$lng->{eptEditB}    = "変更";                  #"Change";
$lng->{eptEditPrvB} = "プレビュー";               #"Preview";
$lng->{eptPrvTtl}   = "プレビュー";               #"Preview";
$lng->{eptDeleted}  = "[削除された]";             #"[deleted]";

# Post attachments page
$lng->{attTitle}    = "添付を投稿";               #"Post Attachments";
$lng->{attDelAll}   = "すべて削除";               #"Delete All";
$lng->{attDelAllTT} = "すべての添付を削除する";         #"Delete all attachments";
$lng->{attDropNote} = "ファイルをフォームにドロップすることでもアップロードできます。"
  ;    #"You can also upload files by dropping them onto the form.";
$lng->{attGoPostT} =
  "上矢印アイコンで投稿に戻ります。";    #"The up-arrow icon leads back to the post.";
$lng->{attUplTtl} = "アップロード";    #"Upload";
$lng->{attUplFiles} =
  "ファイル(最大ファイルサイズ[[size]])";     #"File(s) (max. file size [[size]])";
$lng->{attUplCapt} = "説明";       #"Caption";
$lng->{attUplEmbed} =
  "埋め込み(JPG, PNG, GIF画像のみ)";     #"Embed (only JPG, PNG and GIF images)";
$lng->{attUplB}    = "アップロード";   #"Upload";
$lng->{attAttTtl}  = "添付";       #"Attachment";
$lng->{attAttDelB} = "削除";       #"Delete";
$lng->{attAttChgB} = "変更";       #"Change";

# Attachment page
$lng->{atsTitle}  = "添付";         #"Attachment";
$lng->{atsPrev}   = "前";          #"Previous";
$lng->{atsPrevTT} = "前の添付に行く";    #"Go to previous attachment";
$lng->{atsNext}   = "次";          #"Next";
$lng->{atsNextTT} = "次の添付に行く";    #"Go to next attachment";

# User info page
$lng->{uifTitle}     = "ユーザ";            #"User";
$lng->{uifListPst}   = "投稿";             #"Posts";
$lng->{uifListPstTT} = "ユーザの投稿を表示する";    #"Show user&#39;s posts";
$lng->{uifActiv}     = "活動";             #"Activity";
$lng->{uifActivTT} =
  "1日あたりおよび1年あたりのユーザの投稿を表示する";    #"Show user&#39;s posts per day and year";
$lng->{uifMessage} = "メッセージを送る";  #"Send Message";
$lng->{uifMessageTT} =
  "このユーザにプライベートメッセージを送る";         #"Send private message to this user";
$lng->{uifIgnore}    = "無視";                #"Ignore";
$lng->{uifIgnoreTT}  = "このユーザを無視する";        #"Ignore this user";
$lng->{uifWatch}     = "監視";                #"Watch";
$lng->{uifWatchTT}   = "ユーザを監視リストに置く";      #"Put user on watch list";
$lng->{uifProfTtl}   = "プロファイル";            #"Profile";
$lng->{uifProfUName} = "ユーザ名";              #"Username";
$lng->{uifProfOName} = "古い名前";              #"Old Names";
$lng->{uifProfRName} = "実名";                #"Real Name";
$lng->{uifProfBdate} = "誕生日";               #"Birthday";
$lng->{uifProfPage}  = "ウェブサイト";            #"Website";
$lng->{uifProfOccup} = "職業";                #"Occupation";
$lng->{uifProfHobby} = "趣味";                #"Hobbies";
$lng->{uifProfLocat} = "位置";                #"Location";
$lng->{uifProfGeoIp} = "位置(IPに基く)";         #"Location (IP-based)";
$lng->{uifProfIcq}   = "メール/メッセンジャー";       #"Email/Messengers";
$lng->{uifProfSig}   = "署名";                #"Signature";
$lng->{uifProfBlurb} = "その他";               #"Miscellaneous";
$lng->{uifProfAvat}  = "アバター";              #"Avatar";
$lng->{uifBadges}    = "バッジ";               #"Badges";
$lng->{uifGrpMbrTtl} = "グループ";              #"Groups";
$lng->{uifBrdSubTtl} = "掲示板の購読登録";          #"Board Subscriptions";
$lng->{uifTpcSubTtl} = "トピックの購読登録";         #"Topic Subscriptions";
$lng->{uifStatTtl}   = "統計";                #"Statistics";
$lng->{uifStatRank}  = "ランク";               #"Rank";
$lng->{uifStatPNum}  = "投稿数";               #"Posts";
$lng->{uifStatPONum} = "投稿";                #"posted";
$lng->{uifStatPENum} = "の存在する投稿";           #"existing";
$lng->{uifStatRegTm} = "登録日時";              #"Registered";
$lng->{uifStatLOTm}  = "最後にオンラインだった日時";     #"Last Online";
$lng->{uifStatLRTm}  = "その前にオンラインだった日時";    #"Prev. Online";
$lng->{uifStatLIp}   = "最新のIP";             #"Last IP";
$lng->{uifMapTtl}    = "地図";                #"Map";
$lng->{uifMapOthrMt} = "その他の一致";            #"other matches";

# User list page
$lng->{uliTitle}     = "ユーザリスト";            #"User List";
$lng->{uliLfmTtl}    = "リスト形式";             #"List Format";
$lng->{uliLfmSearch} = "検索";                #"Search";
$lng->{uliLfmField}  = "見るもの";              #"View";
$lng->{uliLfmSort}   = "整列";                #"Sort";
$lng->{uliLfmSrtNam} = "ユーザ名";              #"Username";
$lng->{uliLfmSrtUid} = "ユーザID";             #"User ID";
$lng->{uliLfmSrtFld} = "見るもの";              #"View";
$lng->{uliLfmOrder}  = "順序";                #"Order";
$lng->{uliLfmOrdAsc} = "昇順";                #"Asc";
$lng->{uliLfmOrdDsc} = "降順";                #"Desc";
$lng->{uliLfmHide}   = "空を隠す";              #"Hide empty"; # XXX ?
$lng->{uliLfmListB}  = "リストする";             #"List";
$lng->{uliLstName}   = "ユーザ名";              #"Username";

# User login page
$lng->{lgiTitle}    = "ユーザ";                #"User";
$lng->{lgiLoginTtl} = "ログイン";               #"Login";
$lng->{lgiLoginT} =
"アカウントをまだ持っていないなら、<a href=\"[[regUrl]]\">登録</a>できます。アカウントを登録したばかりなら、メールでパスワードを受け取ってください(スパムフォルダもチェックしてください)。"
  ; #"If you don't have an account yet, you can <a href=\"[[regUrl]]\">register</a> one. If you just registered an account, you should receive the password by email (check your spam folder, too).";
$lng->{lgiLoginName} = "ユーザ名(またはメールアドレス)";    #"Username (or email address)";
$lng->{lgiLoginPwd}  = "パスワード";               #"Password";
$lng->{lgiLoginRmbr} = "このコンピュータに覚えさせる";      #"Remember me on this computer";
$lng->{lgiLoginB}    = "ログイン";                #"Login";
$lng->{lgiFpwTtl}    = "パスワードを忘れた";           #"Forgot Password";
$lng->{lgiFpwT}      = "パスワードをなくしたなら、登録されたメールアドレスにログインチケットを送信させることができます。"
  ; #"If you have lost your password, you can have a login ticket link sent to your registered email address.";
$lng->{lgiFpwEmail} = "メールアドレス";      #"Email address";
$lng->{lgiFpwB}     = "送る";           #"Send";
$lng->{lgiFpwMlSbj} = "パスワードを忘れた";    #"Forgot Password";
$lng->{lgiFpwMlT} =
"以下のチケットリンクを訪れてパスワードなしでログインしてください。それからパスワードを新しいものに変更できます。\n\nセキュリティ上の理由により、チケットリンクは制限時間内の一度だけの利用のみ有効です。また、2回以上要求した場合、最後に要求されたチケットリンクだけが有効です。"
  ; #"Please visit the following ticket link to login without your password. You may then proceed to change your password to a new one.\n\nFor security reasons, the ticket link is only valid for one use and for a limited time. Also, only the last requested ticket link is valid, should you have requested more than one.";

# User OpenID login page
$lng->{oidTitle}     = "ユーザ";                 #"User";
$lng->{oidLoginTtl}  = "OpenIDログイン";          #"OpenID Login";
$lng->{oidLoginUrl}  = "OpenID URL";          #"OpenID URL";
$lng->{oidLoginRmbr} = "このコンピュータに覚えさせる";      #"Remember me on this computer";
$lng->{oidLoginB}    = "ログイン";                #"Login";
$lng->{oidListTtl}   = "許可されたOpenIDプロバイダ";    #"Accepted OpenID Providers";

# User registration page
$lng->{regTitle}  = "ユーザ";                    #"User";
$lng->{regRegTtl} = "アカウントを登録";               #"Register Account";
$lng->{regRegT} =
"すでにアカウントを持っているなら<a href=\"[[logUrl]]\">ログイン</a>ページでログインできます。またそこで、なくしたパスワードを置き換えることができます。"
  ; #"If you already have an account, you can login on the <a href=\"[[logUrl]]\">login</a> page, where you can also replace lost passwords.";
$lng->{regRegName}  = "ユーザ名";                             #"Username";
$lng->{regRegEmail} = "メールアドレス(ログインパスワードがこのアドレスに送られます)"
  ;    #"Email Address (login password will be sent to this address)";
$lng->{regRegEmailV} = "メールアドレスを繰り返す";    #"Repeat Email Address";
$lng->{regRegPwd}    = "パスワード";           #"Password";
$lng->{regRegPwdFmt} = "最小8文字";           #"min. 8 characters";
$lng->{regRegPwdV}   = "パスワードを繰り返す";      #"Repeat Password";
$lng->{regRegB}      = "登録";              #"Register";
$lng->{regMailSubj}  = "登録";              #"Registration";
$lng->{regMailT} =
  "フォーラムアカウントを登録しました。";    #"You have registered a forum account.";
$lng->{regMailName} = "ユーザ名: ";     #"Username: ";
$lng->{regMailPwd}  = "パスワード: ";    #"Password: ";
$lng->{regMailT2} =
"リンクを使ってログイン、または手動でユーザ名とパスワードを使ってログインした後、オプション/パスワードに行ってパスワードをもっと覚えやすいものに変更してください。"
  ; #"After you have logged in using the link or manually using the username and password, please go to Options/Password and change the password to something more memorable.";

# User profile and options pages
$lng->{uopTitle}    = "ユーザ";               #"User";
$lng->{uopPasswd}   = "パスワード";             #"Password";
$lng->{uopPasswdTT} = "パスワードを変更する";        #"Change password";
$lng->{uopName}     = "名前";                #"Name";
$lng->{uopNameTT}   = "ユーザ名を変更する";         #"Change username";
$lng->{uopEmail}    = "メール";               #"Email";
$lng->{uopEmailTT}  = "メールアドレスを変更する";      #"Change email address";
$lng->{uopGroups}   = "グループ";              #"Groups";
$lng->{uopGroupsTT} = "グループに参加または退会する";    #"Join or leave groups";
$lng->{uopBoards}   = "掲示板";               #"Boards";
$lng->{uopBoardsTT} = "掲示板オプションを設定する";     #"Configure board options";
$lng->{uopTopics}   = "トピック";              #"Topics";
$lng->{uopTopicsTT} = "トピックオプションを設定する";    #"Configure topic options";
$lng->{uopAvatar}   = "アバター";              #"Avatar";
$lng->{uopAvatarTT} = "アバター画像を選択する";       #"Select avatar image";
$lng->{uopBadges}   = "バッジ";               #"Badges";
$lng->{uopBadgesTT} = "バッジを選択する";          #"Select badges";
$lng->{uopIgnore}   = "無視";                #"Ignore";
$lng->{uopIgnoreTT} = "他のユーザを無視する";        #"Ignore other users";
$lng->{uopWatch}    = "監視";                #"Watch";
$lng->{uopWatchTT}  = "監視語と監視ユーザを管理する";    #"Manage watched words and users";
$lng->{uopOpenPgp}  = "OpenPGP";           #"OpenPGP";
$lng->{uopOpenPgpTT} =
  "メール暗号化オプションを設定する";    #"Configure email encryption options";
$lng->{uopInfo}      = "情報";            #"Info";
$lng->{uopInfoTT}    = "ユーザ情報を表示する";    #"Show user info";
$lng->{uopProfTtl}   = "プロファイル";        #"Profile";
$lng->{uopProfRName} = "実名";            #"Real Name";
$lng->{uopProfBdate} =
  "誕生日(YYYY-MM-DDまたはMM-DD)";            #"Birthday (YYYY-MM-DD or MM-DD)";
$lng->{uopProfPage}  = "ウェブサイト";        #"Website";
$lng->{uopProfOccup} = "職業";            #"Occupation";
$lng->{uopProfHobby} = "趣味";            #"Hobbies";
$lng->{uopProfLocat} = "地理的位置";         #"Geographic Location";
$lng->{uopProfLocIn} = "[挿入]";          #"[Insert]";
$lng->{uopProfIcq}   = "メール/メッセンジャー";   #"Email/Messengers";
$lng->{uopProfSig}   = "署名";            #"Signature";
$lng->{uopProfSigLt} = "(最大100文字、2行)";  #"(max. 100 characters, 2 lines)";
$lng->{uopProfBlurb} = "その他";           #"Miscellaneous";
$lng->{uopOptTtl}    = "オプション";         #"Options";
$lng->{uopPrefPrivc} = "プライバシー(オンライン状態とIPに基く位置を隠す)"
  ;    #"Privacy (hide online status and IP-based location)";
$lng->{uopPrefNtMsg} = "返信とメッセージ通知もメールで受け取る"
  ;    #"Receive reply and message notifications by email, too";
$lng->{uopPrefNt}    = "返信通知を受け取る";    #"Receive reply notifications";
$lng->{uopDispLang}  = "言語";           #"Language";
$lng->{uopDispTimeZ} = "タイムゾーン";       #"Timezone";
$lng->{uopDispTimeS} = "サーバ";          #"Server";
$lng->{uopDispStyle} = "スタイル";         #"Style";
$lng->{uopDispFFace} = "フォントの形";       #"Font Face";
$lng->{uopDispFSize} =
  "フォントの大きさ(ピクセルで、0 = デフォルト)";         #"Font Size (in pixels, 0 = default)";
$lng->{uopDispIndnt} =
  "インデント(1-10%, 投稿スレッド化について)";         #"Indent (1-10%, for post threading)";
$lng->{uopDispTpcPP} =
  "ページあたりのトピック数(0 = 許される最大を使う)";   #"Topics Per Page (0 = use allowed maximum)";
$lng->{uopDispPstPP} =
  "ページあたりの投稿数(0 = 許される最大を使う)";     #"Posts Per Page (0 = use allowed maximum)";
$lng->{uopDispDescs} = "掲示板の説明を表示する";                #"Show board descriptions";
$lng->{uopDispDeco}  = "装飾を表示する(ユーザタイトル、バッジ、ランクなど)"
  ;    #"Show decoration (user titles, badges, ranks etc.)";
$lng->{uopDispAvas} = "アバターを表示する";         #"Show avatars";
$lng->{uopDispImgs} = "埋め込み画像と動画を表示する";    #"Show embedded images and videos";
$lng->{uopDispSigs} = "署名を表示する";           #"Show signatures";
$lng->{uopDispColl} =
  "新/未読の投稿がないトピックの枝をたたむ";   #"Collapse topic branches without new/unread posts";
$lng->{uopSubmitB} = "変更";  #"Change";

# User password page
$lng->{pwdTitle}  = "ユーザ";                          #"User";
$lng->{pwdChgTtl} = "パスワードを変更";                     #"Change Password";
$lng->{pwdChgT}   = "複数のアカウントで同じパスワードを使ってはいけません。"
  ;    #"Never use the same password for multiple accounts.";
$lng->{pwdChgPwd}    = "パスワード";         #"Password";
$lng->{pwdChgPwdFmt} = "最小8文字";         #"min. 8 characters";
$lng->{pwdChgPwdV}   = "パスワードを繰り返す";    #"Repeat Password";
$lng->{pwdChgB}      = "変更";            #"Change";

# User name page
$lng->{namTitle}  = "ユーザ";              #"User";
$lng->{namChgTtl} = "ユーザ名を変更";          #"Change Username";
$lng->{namChgT} =
"名前の変更によって生じる混乱のため、この機能は十分な理由があるときだけ使ってください(例えばスペルミス、複数のオンラインアカウントの統一化、あるいは成長してからふざけた名前を変更する)。"
  ; #"Due to the confusion caused by name changes, please only use this function for a good reason (e.g. fixing a spelling mistake, unifying the names of multiple online accounts or changing silly names after growing up).";
$lng->{namChgT2} = "あなたは名前をあと<em>[[times]]</em>回変更できます。"
  ;    #"You can rename yourself <em>[[times]]</em> more times.";
$lng->{namChgName} = "ユーザ名";    #"Username";
$lng->{namChgB}    = "変更";      #"Change";

# User email page
$lng->{emlTitle}  = "ユーザ";        #"User";
$lng->{emlChgTtl} = "メールアドレス";    #"Email Address";
$lng->{emlChgT}   = "新しい、または変更されたメールアドレスは、一度そのアドレスに送られた確認メールに対応してから有効になります。"
  ; #"A new or changed email address will only take effect once you have reacted to the verification email sent to that address.";
$lng->{emlChgAddr}   = "メールアドレス";         #"Email Address";
$lng->{emlChgAddrV}  = "メールアドレスを繰り返す";    #"Repeat Email Address";
$lng->{emlChgB}      = "変更";              #"Change";
$lng->{emlChgMlSubj} = "メールアドレス変更";       #"Email Address Change";
$lng->{emlChgMlT} =
"あなたはフォーラムアカウントのメールアドレスの変更を要求しました。そのアドレスの正当性を保証するために、あなたのアカウントは以下のチケットリンクを訪れて初めて更新されます:"
  ; #"You have requested a change of your forum account's email address. To ensure the validity of the address, your account will only be updated once you have visited the following ticket link:";

# User group options page
$lng->{ugrTitle}     = "ユーザ";            #"User";
$lng->{ugrGrpStTtl}  = "グループメンバ権";       #"Group Membership";
$lng->{ugrGrpStAdm}  = "管理者";            #"Admin";
$lng->{ugrGrpStMbr}  = "メンバ";            #"Member";
$lng->{ugrSubmitTtl} = "グループメンバ権を変更";    #"Change Group Membership";
$lng->{ugrChgB}      = "変更";             #"Change";

# User board options page
$lng->{ubdTitle} = "ユーザ";                #"User";
$lng->{ubdSubsT2} =
  "即時購読は選択された掲示板の新しい投稿をあなたにメールで即座に送ります。要約購読はまとめられた投稿を定期的(通常1日ごと)に送ります。"
  ; #"Instant subscriptions send out new posts in the selected board to you by email instantly. Digest subscriptions send out collected posts regularly (usually daily).";
$lng->{ubdBrdStTtl}  = "掲示板オプション";       #"Board Options";
$lng->{ubdBrdStSubs} = "メール購読";          #"Email Subscription";
$lng->{ubdBrdStInst} = "即時";             #"Instant";
$lng->{ubdBrdStDig}  = "要約";             #"Digest";
$lng->{ubdBrdStOff}  = "オフ";             #"Off";
$lng->{ubdBrdStHide} = "隠す";             #"Hide";
$lng->{ubdSubmitTtl} = "掲示板オプションを変更";    #"Change Board Options";
$lng->{ubdChgB}      = "変更";             #"Change";

# User topic options page
$lng->{utpTitle}     = "ユーザ";            #"User";
$lng->{utpTpcStTtl}  = "トピックオプション";      #"Topic Options";
$lng->{utpTpcStSubs} = "メール購読";          #"Email Subscription";
$lng->{ubdTpcStInst} = "即時";             #"Instant";
$lng->{ubdTpcStDig}  = "要約";             #"Digest";
$lng->{ubdTpcStOff}  = "オフ";             #"Off";
$lng->{utpEmpty} =
  "有効にされたオプションのあるトピックはありません。";    #"No topics with enabled options found.";
$lng->{utpSubmitTtl} = "トピックオプションを変更";    #"Change Topic Options";
$lng->{utpChgB}      = "変更";              #"Change";

# Avatar page
$lng->{avaTitle}  = "ユーザ";                #"User";
$lng->{avaUplTtl} = "カスタムアバター";           #"Custom Avatar";
$lng->{avaUplImgExc} =
  "JPG/PNG/GIF画像(アニメーションなし、最大ファイルサイズ[[size]]、正確な大きさ[[width]]x[[height]]ピクセル)"
  ; #"JPG/PNG/GIF image (no animation, max. file size [[size]], exact dimensions [[width]]x[[height]] pixels)";
$lng->{avaUplImgRsz} = "JPG/PNG/GIF画像(アニメーションなし、最大ファイルサイズ[[size]])"
  ;    #"JPG/PNG/GIF image (no animation, max. file size [[size]])";
$lng->{avaUplUplB}  = "アップロード";             #"Upload";
$lng->{avaUplDelB}  = "削除";                 #"Delete";
$lng->{avaGalTtl}   = "アバターギャラリー";          #"Avatar Gallery";
$lng->{avaGalSelB}  = "選択";                 #"Select";
$lng->{avaGalDelB}  = "選択解除";               #"Unselect";
$lng->{avaGrvTtl}   = "Gravatar";           #"Gravatar";
$lng->{avaGrvEmail} = "Gravatarメールアドレス";    #"Gravatar Email Address";
$lng->{avaGrvSelB}  = "選択";                 #"Select";
$lng->{avaGrvDelB}  = "選択解除";               #"Unselect";

# User badges page
$lng->{bdgTitle}     = "ユーザ";               #"User";
$lng->{bdgSelTtl}    = "バッジ";               #"Badges";
$lng->{bdgSubmitTtl} = "バッジを選択";            #"Select Badges";
$lng->{bdgSubmitB}   = "選択";                #"Select";

# User ignore page
$lng->{uigTitle} = "ユーザ";                                       #"User";
$lng->{uigAddT}  = "無視されたユーザからのプライベートメッセージは黙って捨てられ、投稿は隠されます。"
  ; #"Private messages by ignored users will be silently discarded and posts will be hidden.";
$lng->{uigAddTtl}  = "ユーザを無視リストに追加";     #"Add User to Ignore List";
$lng->{uigAddUser} = "ユーザ名";             #"Username";
$lng->{uigAddB}    = "追加";               #"Add";
$lng->{uigRemTtl}  = "ユーザを無視リストから除去";    #"Remove User from Ignore List";
$lng->{uigRemUser} = "ユーザ名";             #"Username";
$lng->{uigRemB}    = "除去";               #"Remove";

# Watch word/user page
$lng->{watTitle}     = "ユーザ";                            #"User";
$lng->{watWrdAddTtl} = "監視語を追加";                         #"Add Watched Word";
$lng->{watWrdAddT}   = "新しい投稿で監視語が述べられると、あなたは通知を受けます。"
  ; #"If a watched word gets mentioned in a new post, you will receive a notification.";
$lng->{watWrdAddWrd} = "監視語";                           #"Word";
$lng->{watWrdAddB}   = "追加";                            #"Add";
$lng->{watWrdRemTtl} = "監視語を除去";                        #"Remove Watched Word";
$lng->{watWrdRemWrd} = "監視語";                           #"Word";
$lng->{watWrdRemB}   = "除去";                            #"Remove";
$lng->{watUsrAddTtl} = "監視ユーザを追加";                      #"Add Watched User";
$lng->{watUsrAddT}   = "監視ユーザが新しい投稿を書くと、あなたは通知を受けます。"
  ;    #"If a watched user writes a new post, you will receive a notification.";
$lng->{watUsrAddUsr} = "ユーザ名";        #"Username";
$lng->{watUsrAddB}   = "追加";          #"Add";
$lng->{watUsrRemTtl} = "監視ユーザを除去";    #"Remove Watched User";
$lng->{watUsrRemUsr} = "ユーザ名";        #"Username";
$lng->{watUsrRemB}   = "除去";          #"Remove";

# Group info page
$lng->{griTitle}     = "グループ";        #"Group";
$lng->{griMembers}   = "メンバ";         #"Members";
$lng->{griMbrTtl}    = "メンバ";         #"Members";
$lng->{griBrdAdmTtl} = "モデレータ権限";     #"Moderator Permissions";
$lng->{griBrdMbrTtl} = "メンバ権限";       #"Member Permissions";

# Group members page
$lng->{grmTitle}   = "グループ";                            #"Group";
$lng->{grmAddTtl}  = "メンバを追加";                          #"Add Members";
$lng->{grmAddUser} = "ユーザ名(複数可。テキスト入力を使う場合セミコロンで区切る)"
  ;    #"Usernames (separate with semicolons if using text input)";
$lng->{grmAddB}    = "追加";        #"Add";
$lng->{grmRemTtl}  = "メンバを除去";    #"Remove Members";
$lng->{grmRemUser} = "ユーザ名";      #"Username";
$lng->{grmRemB}    = "除去";        #"Remove";

# Board groups page
$lng->{bgrTitle}     = "掲示板";      #"Board";
$lng->{bgrPermTtl}   = "権限";       #"Permissions";
$lng->{bgrModerator} = "モデレータ";    #"Moderator";
$lng->{bgrMember}    = "メンバ";      #"Member";
$lng->{bgrChangeTtl} = "権限を変更";    #"Change Permissions";
$lng->{bgrChangeB}   = "変更";       #"Change";

# Board split page
$lng->{bspTitle}     = "掲示板";        #"Board";
$lng->{bspSplitTtl}  = "掲示板を分割";     #"Split Board";
$lng->{bspSplitDest} = "行き先の掲示板";    #"Destination Board";
$lng->{bspSplitB}    = "分割";         #"Split";

# Topic tag page
$lng->{ttgTitle}  = "トピック";          #"Topic";
$lng->{ttgTagTtl} = "トピックにタグ付け";     #"Tag Topic";
$lng->{ttgTagB}   = "タグ";            #"Tag";

# Topic move page
$lng->{mvtTitle}   = "トピック";         #"Topic";
$lng->{mvtMovTtl}  = "トピックを移動";      #"Move Topic";
$lng->{mvtMovDest} = "行き先の掲示板";      #"Destination Board";
$lng->{mvtMovB}    = "移動";           #"Move";

# Topic merge page
$lng->{mgtTitle}    = "トピック";                             #"Topic";
$lng->{mgtMrgTtl}   = "トピックを統合";                          #"Merge Topics";
$lng->{mgtMrgDest}  = "行き先のトピック";                         #"Destination Topic";
$lng->{mgtMrgDest2} = "代わりに手動でID入力(古いトピックや他の掲示板のトピック用)"
  ; #"Alternative manual ID input (for older topics or topics in other boards)";
$lng->{mgtMrgB} = "統合";    #"Merge";

# Branch page
$lng->{brnTitle}     = "トピックの枝";           #"Topic Branch";
$lng->{brnPromoTtl}  = "トピックに昇格";          #"Promote to Topic";
$lng->{brnPromoSbj}  = "題名";               #"Subject";
$lng->{brnPromoBrd}  = "掲示板";              #"Board";
$lng->{brnPromoLink} = "クロスリンク投稿を追加";      #"Add crosslink posts";
$lng->{brnPromoB}    = "昇格";               #"Promote";
$lng->{brnProLnkBdy} = "トピックの枝は移動しました";    #"topic branch moved";
$lng->{brnMoveTtl}   = "移動";               #"Move";
$lng->{brnMovePrnt}  = "親投稿ID(異なるトピックのものでもよい、0 = このトピックのトップレベルに移動)"
  ; #"Parent post ID (can be in different topic, 0 = move to top level in this topic)";
$lng->{brnMoveB}     = "移動";      #"Move";
$lng->{brnLockTtl}   = "凍結";      #"Lock";
$lng->{brnLockLckB}  = "凍結";      #"Lock";
$lng->{brnLockUnlB}  = "凍結解除";    #"Unlock";
$lng->{brnDeleteTtl} = "削除";      #"Delete";
$lng->{brnDeleteB}   = "削除";      #"Delete";

# Search page
$lng->{seaTitle}     = "フォーラム";                         #"Forum";
$lng->{seaTtl}       = "検索";                            #"Search";
$lng->{seaAdvOpt}    = "もっと";                           #"More";
$lng->{seaBoard}     = "掲示板";                           #"Board";
$lng->{seaBoardAll}  = "すべての掲示板";                       #"All boards";
$lng->{seaWords}     = "検索語";                           #"Words";
$lng->{seaWordsFtsT} = "使われる全文検索式: <em>[[expr]]</em>"
  ;    #"Used fulltext search expression: <em>[[expr]]</em>";
$lng->{seaUser}      = "ユーザ";      #"User";
$lng->{seaMinAge}    = "最小古さ";     #"Min. Age"; # XXX
$lng->{seaMaxAge}    = "最大古さ";     #"Max. Age"; # XXX
$lng->{seaField}     = "フィールド";    #"Field";
$lng->{seaFieldBody} = "本文";       #"Text";
$lng->{seaFieldRaw}  = "生テキスト";    #"Raw Text";
$lng->{seaFieldSubj} = "題名";       #"Subject";
$lng->{seaOrder}     = "順序";       #"Order";
$lng->{seaOrderAsc}  = "古い順";      #"Oldest first";
$lng->{seaOrderDesc} = "新しい順";     #"Newest first";
$lng->{seaB}         = "検索";       #"Search";
$lng->{seaGglTtl} =
  "検索 - Google&trade;を利用";         #"Search - powered by Google&trade;";
$lng->{serTopic}    = "トピック";               #"Topic";
$lng->{serNotFound} = "一致するものが見付かりません。";    #"No matches found.";

# Help page
$lng->{hlpTitle}  = "ヘルプ";                  #"Help";
$lng->{hlpTxtTtl} = "用語と機能";                #"Terms and Features";
$lng->{hlpFaqTtl} = "よくある質問";               #"Frequently Asked Questions";

# Policy page
$lng->{plcRead}    = "私は上記を読んで理解しました";  #"I have read and understood the above";
$lng->{plcAcceptB} = "承諾";              #"Accept";
$lng->{plcRejectB} = "拒否";              #"Reject";

# Message list page
$lng->{mslTitle}    = "プライベートメッセージ";       #"Private Messages";
$lng->{mslSend}     = "送信";                #"Send";
$lng->{mslSendTT}   = "プライベートメッセージを送る";    #"Send private message";
$lng->{mslExport}   = "エクスポート";            #"Export";
$lng->{mslExportTT} = "すべてのプライベートメッセージをひとつのHTMLファイルとしてエクスポートする"
  ;    #"Export all private messages as one HTML file";
$lng->{mslDelAll} = "削除";    #"Delete";
$lng->{mslDelAllTT} =
  "すべての既読および送信済みプライベートメッセージを削除する"; #"Delete all read and sent private messages";
$lng->{mslInbox}    = "受信箱";       #"Inbox";
$lng->{mslOutbox}   = "送信箱";       #"Sent";
$lng->{mslFrom}     = "差出人";       #"Sender";
$lng->{mslTo}       = "受取人";       #"Recipient";
$lng->{mslDate}     = "日時";        #"Date";
$lng->{mslCommands} = "コマンド";      #"Commands";
$lng->{mslDelete}   = "削除";        #"Delete";
$lng->{mslNotFound} =
  "この箱にはプライベートメッセージはありません。";       #"No private messages in this box.";
$lng->{mslExpire} = "プライベートメッセージは[[days]]日後に期限切れになります。"
  ;    #"Private messages expire after [[days]] days.";

# Add message page
$lng->{msaTitle}     = "プライベートメッセージ";       #"Private Message";
$lng->{msaSendTtl}   = "プライベートメッセージを送る";    #"Send Private Message";
$lng->{msaSendRecv}  = "受取人";               #"Recipient";
$lng->{msaSendRecvM} = "受取人(複数可。最大[[maxRcv]]個の名前をセミコロンで区切って)"
  ;    #"Recipients (separate up to [[maxRcv]] names with semicolons)";
$lng->{msaSendSbj}   = "題名";         #"Subject";
$lng->{msaSendTxt}   = "メッセージ本文";    #"Message Text";
$lng->{msaSendB}     = "送信";         #"Send";
$lng->{msaSendPrvB}  = "プレビュー";      #"Preview";
$lng->{msaPrvTtl}    = "プレビュー";      #"Preview";
$lng->{msaRefTtl}    = "以下への返信:";    #"In Response to"; # XXX parameter required
$lng->{msaEmailSbPf} = "次の人からのメッセージ";  #"Message from"; # XXX parameter required
$lng->{msaEmailTSbj} = "題名";           #"Subject: ";
$lng->{msaEmailT2}   = "これはフォーラムソフトウェアからのメッセージ通知です。\nこのメールには返信しないでください。"
  ; #"This is a message notification from the forum software.\nPlease don't reply to this email.";

# Message page
$lng->{mssTitle}    = "プライベートメッセージ";        #"Private Message";
$lng->{mssDelete}   = "削除";                 #"Delete";
$lng->{mssDeleteTT} = "メッセージを削除";           #"Delete message";
$lng->{mssReply}    = "返信";                 #"Reply";
$lng->{mssReplyTT}  = "メッセージに返信する";         #"Reply to message";
$lng->{mssQuote}    = "引用";                 #"Quote";
$lng->{mssQuoteTT}  = "引用付きでメッセージに返信する";    #"Reply to message with quote";
$lng->{mssFrom}     = "差出人";                #"From";
$lng->{mssTo}       = "受取人";                #"To";
$lng->{mssDate}     = "日時";                 #"Date";
$lng->{mssSubject}  = "題名";                 #"Subject";

# Chat page
$lng->{chtTitle}     = "チャット";              #"Chat";
$lng->{chtRefresh}   = "更新";                #"Refresh";
$lng->{chtRefreshTT} = "ページを更新する";          #"Refresh page";
$lng->{chtDelAll}    = "すべて削除";             #"Delete All";
$lng->{chtDelAllTT}  = "すべてのメッセージを削除する";    #"Delete all messages";
$lng->{chtAddTtl}    = "メッセージを投稿";          #"Post Message";
$lng->{chtAddB}      = "投稿";                #"Post";
$lng->{chtMsgsTtl}   = "メッセージ";             #"Messages";

# Attachment list page
$lng->{aliTitle}     = "添付リスト";             #"Attachment List";
$lng->{aliLfmTtl}    = "検索";                #"Search and Format"; # XXX format?
$lng->{aliLfmWords}  = "検索語";               #"Words";
$lng->{aliLfmUser}   = "ユーザ";               #"User";
$lng->{aliLfmBoard}  = "掲示板";               #"Board";
$lng->{aliLfmField}  = "フィールド";             #"Field";
$lng->{aliLfmFldFnm} = "ファイル名";             #"Filename";
$lng->{aliLfmFldCpt} = "説明";                #"Caption";
$lng->{aliLfmMinAge} = "最小古さ";              #"Min. Age";
$lng->{aliLfmMaxAge} = "最大古さ";              #"Max. Age";
$lng->{aliLfmOrder}  = "順序";                #"Order";
$lng->{aliLfmOrdAsc} = "古い順";               #"Oldest first";
$lng->{aliLfmOrdDsc} = "新しい順";              #"Newest first";
$lng->{aliLfmGall}   = "ギャラリー";             #"Gallery";
$lng->{aliLfmListB}  = "リスト";               #"List";
$lng->{aliLstFile}   = "ファイル名";             #"Filename";
$lng->{aliLstCapt}   = "説明";                #"Caption";
$lng->{aliLstSize}   = "サイズ";               #"Size";
$lng->{aliLstPost}   = "投稿";                #"Post";
$lng->{aliLstUser}   = "ユーザ";               #"User";

# Feeds page
$lng->{fedTitle}     = "フィード";              #"Feeds";
$lng->{fedAllBoards} = "すべての公開掲示板";         #"All public boards";

# Email subscriptions
$lng->{subSubjBrdIn} = "掲示板即時購読";           #"Board instant subscription";
$lng->{subSubjTpcIn} = "トピック即時購読";          #"Topic instant subscription";
$lng->{subSubjBrdDg} = "掲示板要約購読";           #"Board digest subscription";
$lng->{subSubjTpcDg} = "トピック要約購読";          #"Topic digest subscription";
$lng->{subNoReply}   = "これはフォーラムソフトウェアからの購読メールです。\nこのメールには返信しないでください。"
  ; #"This is a subscription email from the forum software.\nPlease don't reply to this email.";
$lng->{subLink}     = "リンク: ";                #"Link: ";
$lng->{subBoard}    = "掲示板: ";                #"Board: ";
$lng->{subTopic}    = "トピック: ";               #"Topic: ";
$lng->{subBy}       = "ユーザ: ";                #"User: ";
$lng->{subOn}       = "日時: ";                 #"Date: ";
$lng->{subUnsubBrd} = "この掲示板の購読登録を解除する:";     #"Unsubscribe from this board:";
$lng->{subUnsubTpc} = "このトピックの購読登録を解除する:";    #"Unsubscribe from this topic:";

# Bounce detection
$lng->{bncWarning} =
"警告: あなたのメールアカウントはもはや存在しないか、メールを拒否しているか、または自動返信のスパムかのいずれかです。この状況を修正してください。そうでなければフォーラムはあなたにメールを送るのをやめなければならないでしょう。"
  ; #"Warning: either your email account doesn't exist anymore, rejects emails, or spams with automatic replies. Please rectify this situation, or the forum may have to stop sending email to you.";

# Confirmation
$lng->{cnfTitle} = "確認";    #"Confirmation";
$lng->{cnfDelAllMsg} =
  "本当にすべての既読メッセージを削除しますか？";  #"Do you really want to delete all read messages?";
$lng->{cnfDelAllCht} = "本当にすべてのチャットメッセージを削除しますか？"
  ;                          #"Do you really want to delete all chat messages?";
$lng->{cnfDelAllAtt} =
  "本当にすべての添付を削除しますか？";       #"Do you really want to delete all attachments?";
$lng->{cnfQuestion} =
  "本当に削除しますか";    #"Do you really want to delete"; # XXX parameter required
$lng->{cnfQuestion2} = " を？";      #"?"; # XXX parameter required
$lng->{cnfTypeUser}  = "ユーザ";      #"user";
$lng->{cnfTypeGroup} = "グループ";     #"group";
$lng->{cnfTypeCateg} = "カテゴリ";     #"category";
$lng->{cnfTypeBoard} = "掲示板";      #"board";
$lng->{cnfTypeTopic} = "トピック";     #"topic";
$lng->{cnfTypePoll}  = "投票";       #"poll";
$lng->{cnfTypePost}  = "投稿";       #"post";
$lng->{cnfTypeMsg}   = "メッセージ";    #"message";
$lng->{cnfDeleteB}   = "削除";       #"Delete";

# Notification messages
$lng->{notNotify} =
  "ユーザに通知する(任意で理由を指定する)";          #"Notify user (optionally specify reason)";
$lng->{notReason} = "理由:";         #"Reason:";
$lng->{notMsgAdd} = "[[usrNam]]がプライベート<a href=\"[[msgUrl]]\">メッセージ</a>を送信しました。"
  ;    #"[[usrNam]] sent a private <a href=\"[[msgUrl]]\">message</a>.";
$lng->{notPstAdd} = "[[usrNam]]が<a href=\"[[pstUrl]]\">投稿</a>に返信しました。"
  ;    #"[[usrNam]] replied to a <a href=\"[[pstUrl]]\">post</a>.";
$lng->{notPstPng} = "[[usrNam]]があなたに<a href=\"[[pstUrl]]\">投稿</a>について通知しました。"
  ;    #"[[usrNam]] has notified you about a <a href=\"[[pstUrl]]\">post</a>.";
$lng->{notPstEdt} = "モデレータが<a href=\"[[pstUrl]]\">投稿</a>を編集しました。"
  ;    #"A moderator edited a <a href=\"[[pstUrl]]\">post</a>.";
$lng->{notPstDel} = "モデレータが<a href=\"[[tpcUrl]]\">投稿</a>を削除しました。"
  ;    #"A moderator deleted a <a href=\"[[tpcUrl]]\">post</a>.";
$lng->{notTpcMov} = "モデレータが<a href=\"[[tpcUrl]]\">トピック</a>を移動しました。"
  ;    #"A moderator moved a <a href=\"[[tpcUrl]]\">topic</a>.";
$lng->{notTpcDel} = "モデレータが\"[[tpcSbj]]\"というタイトルのトピックを削除しました。"
  ;    #"A moderator deleted a topic titled \"[[tpcSbj]]\".";
$lng->{notTpcMrg} = "モデレータがトピックを他の<a href=\"[[tpcUrl]]\">トピック</a>に統合しました。"
  ; #"A moderator merged a topic into another <a href=\"[[tpcUrl]]\">topic</a>.";
$lng->{notEmlReg} =
"ようこそ、[[usrNam]]！ メールを使った機能を有効にするには、あなたの<a href=\"[[emlUrl]]\">メールアドレス</a>を入力してください。"
  ; #"Welcome, [[usrNam]]! To enable email-based features, please enter your <a href=\"[[emlUrl]]\">email address</a>.";
$lng->{notOidRen} =
"あなたを自動的に短いユーザ名に割り当てることができなかったので、あなたは自分の<a href=\"[[namUrl]]\">名前を付け直す</a>こともできます。"
  ; #"As it wasn't possible to automatically assign you a short username, you may optionally <a href=\"[[namUrl]]\">rename</a> yourself.";
$lng->{notWatWrd} = "監視語\"[[watWrd]]\"が<a href=\"[[pstUrl]]\">投稿</a>で述べられました。"
  ; #"Watched word \"[[watWrd]]\" was mentioned in a <a href=\"[[pstUrl]]\">post</a>.";
$lng->{notWatUsr} = "監視ユーザ\"[[watUsr]]\"が<a href=\"[[pstUrl]]\">投稿</a>を書きました。"
  ;    #"Watched user \"[[watUsr]]\" wrote a <a href=\"[[pstUrl]]\">post</a>.";
$lng->{notThrStr} =
"あなたは間違った<a href=\"[[pstUrl]]\">投稿</a>に返信したようです。でたらめな返信ボタンではなくて、あなたが参照している投稿の特定の返信ボタンを使ってください。これはトピックのスレッド構造を保つために重要なことで、また返信通知を正しい相手に送るのを確実にすることにもなります。特定の投稿を参照せずにトピック全般に返信したいときは、ページの上端と下端辺りにあるトピックレベルの投稿ボタンを使ってください。"
  ; #"You seem to have replied to the wrong <a href=\"[[pstUrl]]\">post</a>. Please use the specific Reply button of the post that you are referencing, not just any random Reply button. This is important to preserve the threaded tree structure of topics, and also makes sure that reply notifications go to the right person. If you want to reply to a topic in general without referencing a specific post, use the topic-level Post button near the top and bottom of the page.";

# Execution messages
$lng->{msgReplyPost} = "返信が投稿されました";           #"Reply posted";
$lng->{msgNewPost}   = "新しいトピックが投稿されました";      #"New topic posted";
$lng->{msgPstChange} = "投稿が変更されました";           #"Post changed";
$lng->{msgPstDel}    = "投稿が削除されました";           #"Post deleted";
$lng->{msgPstTpcDel} = "投稿とトピックが削除されました";      #"Post and topic deleted";
$lng->{msgPstApprv}  = "投稿が承認されました";           #"Post approved";
$lng->{msgPstAttach} = "添付が追加されました";           #"Attachment(s) added";
$lng->{msgPstDetach} = "添付が削除されました";           #"Attachment(s) deleted";
$lng->{msgPstAttChg} = "添付が変更されました";           #"Attachment changed";
$lng->{msgEmlChange} = "確認メールが送信されました";        #"Verification email sent";
$lng->{msgPrfChange} = "プロファイルが変更されました";       #"Profile changed";
$lng->{msgOptChange} = "オプションが変更されました";        #"Options changed";
$lng->{msgPwdChange} = "パスワードが変更されました";        #"Password changed";
$lng->{msgNamChange} = "ユーザ名が変更されました";         #"Username changed";
$lng->{msgAvaChange} = "アバターが変更されました";         #"Avatar changed";
$lng->{msgBdgChange} = "バッジが変更されました";          #"Badges changed";
$lng->{msgGrpChange} = "グループメンバ権が変更されました";     #"Group memberships changed";
$lng->{msgBrdChange} = "掲示板オプションが変更されました";     #"Board options changed";
$lng->{msgTpcChange} = "トピックオプションが変更されました";    #"Topic options changed";
$lng->{msgAccntReg}  = "アカウントが登録されました";        #"Account registered";
$lng->{msgAccntRegM} =
"アカウントが登録されました。ログインに進む前にパスワードが書かれたメールが届くのを待ってください。メールはスパムフォルダに行き着くかもしれないし、対スパム判定が少しの間遅らせるかもしれません。"
  ; #"Account registered. Please wait for the email with your password to arrive before proceeding to login. The email may end up in your spam folder, and anti-spam measures may delay it for some time.";
$lng->{msgMemberAdd} = "メンバが追加されました";            #"Member(s) added";
$lng->{msgMemberRem} = "メンバが除去されました";            #"Member(s) removed";
$lng->{msgTpcDelete} = "トピックが削除されました";           #"Topic deleted";
$lng->{msgTpcStik}   = "トピックが固定に変更されました";        #"Topic changed to sticky";
$lng->{msgTpcUnstik} = "トピックの固定が解除されました";        #"Topic changed to not sticky";
$lng->{msgTpcLock}   = "トピックが凍結されました";           #"Topic locked";
$lng->{msgTpcUnlock} = "トピックの凍結が解除されました";        #"Topic unlocked";
$lng->{msgTpcMove}   = "トピックが移動されました";           #"Topic moved";
$lng->{msgTpcMerge}  = "トピックが統合されました";           #"Topics merged";
$lng->{msgBrnPromo}  = "枝が昇格されました";              #"Branch promoted";
$lng->{msgBrnMove}   = "枝が移動されました";              #"Branch moved";
$lng->{msgBrnDelete} = "枝が削除されました";              #"Branch deleted";
$lng->{msgPstAddRep} = "投稿が報告されました";             #"Post reported";
$lng->{msgPstRemRep} = "報告が削除されました";             #"Report deleted";
$lng->{msgMarkOld}   = "投稿が[古い]にされました";          #"Posts marked as old";
$lng->{msgMarkRead}  = "投稿が既読にされました";            #"Posts marked as read";
$lng->{msgPollAdd}   = "投票が追加されました";             #"Poll added";
$lng->{msgPollDel}   = "投票が削除されました";             #"Poll deleted";
$lng->{msgPollLock}  = "投票が締め切られました";            #"Poll closed";
$lng->{msgPollVote}  = "投票されました";                #"Voted";
$lng->{msgMsgAdd}    = "プライベートメッセージが送信されました";    #"Private message sent";
$lng->{msgMsgDel}    = "プライベートメッセージが削除されました";    #"Private message(s) deleted";
$lng->{msgChatAdd}   = "チャットメッセージが追加されました";      #"Chat message added";
$lng->{msgChatDel}   = "チャットメッセージが削除されました";      #"Chat message(s) deleted";
$lng->{msgIgnoreAdd} = "無視ユーザが追加されました";          #"Ignored user added";
$lng->{msgIgnoreRem} = "無視ユーザが除去されました";          #"Ignored user removed";
$lng->{msgWatWrdAdd} = "監視語が追加されました";            #"Watched word added";
$lng->{msgWatWrdRem} = "監視語が除去されました";            #"Watched word removed";
$lng->{msgWatUsrAdd} = "監視ユーザが追加されました";          #"Watched user added";
$lng->{msgWatUsrRem} = "監視ユーザが除去されました";          #"Watched user removed";
$lng->{msgTksFgtPwd} = "メールが送信されました";            #"Email sent";
$lng->{msgTkaFgtPwd} = "ログインしました。今パスワードを変更することができます。"
  ;    #"Logged in. You may now change your password.";
$lng->{msgTkaEmlChg} = "メールアドレスが変更されました";      #"Email address changed";
$lng->{msgTpcTag}    = "トピックがタグ付けられました";       #"Topic tagged";
$lng->{msgTpcSub}    = "トピックの購読が登録されました";      #"Topic subscribed";
$lng->{msgTpcUnsub}  = "トピックの購読登録が解除されました";    #"Topic unsubscribed";
$lng->{msgBrdUnsub}  = "掲示板の購読登録が解除されました";     #"Board unsubscribed";
$lng->{msgNotesDel}  = "通知が削除されました";           #"Notifications deleted";
$lng->{msgPstLock}   = "投稿が凍結されました";           #"Post locked";
$lng->{msgPstUnlock} = "投稿の凍結が解除されました";        #"Post unlocked";
$lng->{msgPstPing}   = "投稿通知が送信されました";         #"Post notification sent";
$lng->{msgPstLike}   = "投稿に[いいね]が付けられました";     #"Post upvoted";
$lng->{msgPstUnlike} = "投稿の[いいね]が除去されました";     #"Post upvote removed";

# Error messages
$lng->{errDefault}   = "[エラー文字列がありません]";    #"[error string missing]";
$lng->{errParamMiss} = "必須のパラメタがありません。";    #"Mandatory parameter is missing.";
$lng->{errCatNotFnd} = "カテゴリが存在しません。";      #"Category doesn't exist.";
$lng->{errBrdNotFnd} = "掲示板が存在しません。";       #"Board doesn't exist.";
$lng->{errTpcNotFnd} = "トピックが存在しません。";      #"Topic doesn't exist.";
$lng->{errPstNotFnd} = "投稿が存在しません。";        #"Post doesn't exist.";
$lng->{errAttNotFnd} = "添付が存在しません。";        #"Attachment doesn't exist.";
$lng->{errMsgNotFnd} = "メッセージが存在しません。";     #"Message doesn't exist.";
$lng->{errUsrNotFnd} = "ユーザが存在しません。";       #"User doesn't exist.";
$lng->{errGrpNotFnd} = "グループが存在しません。";      #"Group doesn't exist.";
$lng->{errTktNotFnd} =
  "チケットが存在しません。チケットは一度だけ使えて、2日後に期限切れになり、また最も最近要求されたチケットだけが有効です。"
  ; #"Ticket doesn't exist. Tickets only work once, expire after two days, and only the most recently requested ticket is valid.";
$lng->{errUnsNotFnd} =
  "登録コードが存在しません。";    #"Subscription code doesn't exist."; # XXX ?
$lng->{errUsrDel} =
  "ユーザアカウントはもう存在しません。";    #"User account doesn't exist anymore.";
$lng->{errUsrFake} = "本当のユーザアカウントではありません。"; #"Not a real user account."; # XXX ?
$lng->{errSubEmpty} = "題名が空です。";            #"Subject is empty.";
$lng->{errBdyEmpty} = "本文が空です。";            #"Text is empty.";
$lng->{errNamEmpty} = "名前が空です。";            #"Name is empty.";
$lng->{errPwdEmpty} = "パスワードが空です。";         #"Password is empty.";
$lng->{errEmlEmpty} = "メールアドレスが空です。";       #"Email address is empty.";
$lng->{errEmlInval} = "メールアドレスが無効です。";      #"Email address is invalid.";
$lng->{errNamSize}  = "名前が短すぎるか長すぎます。";     #"Name is too short or too long.";
$lng->{errPwdSize} =
  "パスワードは少なくとも8文字必要です。";    #"Password needs to have at least 8 characters.";
$lng->{errEmlSize} =
  "メールアドレスが短すぎるか長すぎます。";    #"Email address is too short or too long.";
$lng->{errNamChar} = "名前が不正な文字を含んでいます。";   #"Name contains illegal characters.";
$lng->{errPwdChar} =
  "パスワードが不正な文字を含んでいます。";    #"Password contains illegal characters.";
$lng->{errPwdWrong}  = "パスワードが違います。";        #"Password is wrong.";
$lng->{errNoAccess}  = "アクセスが拒否されました。";      #"Access denied.";
$lng->{errBannedT}   = "あなたは追放されました。理由:";    #"You have been banned. Reason:";
$lng->{errBannedT2}  = "期間: ";               #"Duration: ";
$lng->{errBannedT3}  = "日間。";                #"days.";
$lng->{errBlockEmlT} = "あなたのメールドメインはフォーラムのブラックリストにあります。"
  ;    #"Your email domain is on the forum's blacklist.";
$lng->{errBlockIp} = "あなたのIPアドレスはフォーラムのブラックリストにあります。"
  ;    #"Your IP address is on the forum's blacklist.";
$lng->{errSubLen} = "最大の題名の長さを超えました。";    #"Max. subject length exceeded.";
$lng->{errBdyLen} = "最大の本文の長さを超えました。";    #"Max. text length exceeded.";
$lng->{errOptLen} =
  "最大の選択肢の長さを超えました。";    #"Max. option length exceeded."; # XXX counts?
$lng->{errTpcLocked} = "トピックは凍結されています。";    #"Topic is locked.";
$lng->{errPstLocked} = "投稿は凍結されています。";      #"Post is locked.";
$lng->{errSubNoText} =
  "題名が実際のテキストをまったく含んでいません。";    #"Subject doesn't contain any real text.";
$lng->{errNamGone}   = "名前はすでに登録されています。";       #"Name is already registered.";
$lng->{errNamResrvd} = "名前は予約されたテキストを含んでいます。";  #"Name contains reserved text.";
$lng->{errEmlGone}   = "メールアドレスはすでに登録されています。ひとつのアドレスにはひとつのアカウントのみ。"
  ;    #"Email address is already registered. Only one account per address.";
$lng->{errPwdDiffer} = "パスワードが異なります。";      #"Passwords differ.";
$lng->{errEmlDiffer} = "メールアドレスが異なります。";    #"Email addresses differ.";
$lng->{errDupe} = "この投稿はすでに投稿されています。";    #"This post has already been posted.";
$lng->{errAttName} =
  "ファイルまたはファイル名が指定されていません。";              #"No file or filename specified.";
$lng->{errAttSize} = "アップロードがありません。切り詰められたか許される最大サイズを超えたかしました。"
  ;    #"Upload is missing, was truncated or exceeds maximum allowed size.";
$lng->{errPromoTpc} =
  "この投稿はトピック全体の土台となる投稿です。";  #"This post is the base post for the whole topic.";
$lng->{errPstEdtTme} = "投稿はそれ自体の送信後限られた時間だけ編集できます。この制限時間が期限切れになっています。"
  ; #"Posts may only be edited a limited time after their original submission. This time limit has expired.";
$lng->{errDontEmail} =
"あなたのアカウントへのメール送信は無効にされています。典型的な理由は無効なメールアドレス、メールボックスがいっぱい、あるいは自動返信が作動している、です。"
  ; #"Sending of email for your account has been disabled. Typical reasons are invalid email addresses, jammed mailboxes and activated autoresponders.";
$lng->{errEditAppr} = "モデレートされた掲示板の投稿は一度承認されるともはやあなたは編集できません。"
  ; #"You can't edit posts in a moderated board anymore once they're approved.";
$lng->{errRepDupe} =
  "あなたはすでにこの投稿を報告しています。";    #"You have already reported this post.";
$lng->{errRepReason} = "理由欄が空です。";    #"Reason field is empty.";
$lng->{errSrcAuth} =
"発信元認証要求に失敗しました。誰かがあなたをごまかして、あなたがしようとしていないことをさせようとしたか(あなたがこのページに異なるサイトから来たなら)、あるいはあなたがフォーラムページをあまりにも長く開いたままにしていたかです。"
  ; #"Request source authentication failed. Either someone tried tricking you into doing something that you didn't want to do (if you came to this page from a different site), or you left a forum page open for too long.";
$lng->{errPolExist} = "トピックにはすでに投票があります。";    #"Topic already has a poll.";
$lng->{errPolOptNum} =
  "投票の選択肢が少なすぎるか多すぎます。";    #"Poll has too few or too many options.";
$lng->{errPolNoDel} =
  "票を投じられていない投票だけが削除できます。";    #"Only polls without votes can be deleted.";
$lng->{errPolNoOpt}  = "選択肢が選択されていません。";    #"No option selected.";
$lng->{errPolNotFnd} = "投票が存在しません。";        #"Poll doesn't exist.";
$lng->{errPolLocked} = "投票は終了しています。";       #"Poll is closed.";
$lng->{errPolOpNFnd} = "投票の選択肢が存在しません。";    #"Poll option doesn't exist.";
$lng->{errPolVotedP} =
  "あなたはすでにこの投票に投票しています。";    #"You have already voted in this poll.";
$lng->{errAvaSizeEx} = "最大ファイルサイズを超えました。";    #"Maximum file size exceeded.";
$lng->{errAvaDimens} =
  "画像は指定された幅と高さでなければなりません。";    #"Image must have specified width and height.";
$lng->{errAvaFmtUns} =
  "ファイル形式がサポートされていないか無効です。";    #"File format unsupported or invalid.";
$lng->{errAvaNoAnim} =
  "アニメーション画像は許されていません。";        #"Animated images are not allowed.";
$lng->{errRepostTim} =
  "送り過ぎの制御が有効になっています。あなたはまた投稿できるようになるまで[[seconds]]秒待たなければなりません。"
  ; #"Flood control enabled. You have to wait [[seconds]] seconds before you can post again.";
$lng->{errCrnEmuBsy} = "フォーラムは現在メンテナンス作業で忙しい状態です。後でまた来てください。"
  ; #"The forum is currently busy with maintenance tasks. Please come back later.";
$lng->{errForumLock} = "フォーラムは現在凍結されています。後でまた来てください。"
  ;    #"The forum is currently locked. Please come back later.";
$lng->{errMinRegTim} = "この機能を使えるようになるには登録されてから少なくとも[[hours]]時間必要です。"
  ; #"You need to be registered for at least [[hours]] hour(s) before you can use this feature.";
$lng->{errDbHidden} =
  "データベースエラーが起こり、それは記録されました。"; #"A database error has occurred and was logged.";
$lng->{errCptTmeOut} = "対スパム画像は時間切れになりました。フォームを送信するまでの猶予は[[seconds]]秒です。"
  ; #"Anti-spam image timed out, you have [[seconds]] seconds to submit the form.";
$lng->{errCptWrong} = "対スパム画像からの文字列が正しくありません。もう一度やってください。"
  ;   #"Characters from the anti-spam image are not correct. Please try again.";
$lng->{errCptFail}   = "あなたは対スパムテストに失敗しました。"; #"You failed the anti-spam test.";
$lng->{errOidEmpty}  = "OpenID URLが空です。";     #"OpenID URL is empty.";
$lng->{errOidLen}    = "OpenID URLが長すぎます。";   #"OpenID URL is too long.";
$lng->{errOidPrNtAc} = "OpenIDプロバイダが、認められたプロバイダのリストにありません。"
  ;    #"OpenID provider is not on the list of accepted providers.";
$lng->{errOidNotFnd} =
  "OpenID URLまたはプロバイダが見付かりません。";    #"OpenID URL or provider not found.";
$lng->{errOidCancel} =
  "OpenID確認がユーザによって取り消されました。";      #"OpenID verification cancelled by user.";
$lng->{errOidReplay} =
  "OpenIDリプレイ攻撃が検出されました。";          #"OpenID replay attack detected.";
$lng->{errOidFail}  = "OpenID確認が失敗しました。";  #"OpenID verification failed.";
$lng->{errWordSize} = "監視語が短すぎるか長すぎます。";   #"Word is too short or too long.";
$lng->{errWordChar} = "監視語が不正な文字を含んでいます。"; #"Word contains illegal characters.";
$lng->{errWatchNum} =
  "最大数の監視エントリが使われています。";    #"Maximum number of watch entries used.";
$lng->{errFgtPwdDuh} =
  "あなたは最近すでにこの機能を使ったか、登録したばかりです。そのメールが届くのを待ってください。また必ずスパムフォルダもチェックしてください。"
  ; #"You have already used this function recently or you have only just registered. Please wait for the email to arrive, and make sure to also check your spam folder.";
$lng->{errRecvNum}  = "受取人が多すぎます。";    #"Too many recipients.";
$lng->{errOldAgent} = "あなたのブラウザはとても古く、このフォーラムではもはやサポートされていません。最新のブラウザを入手してください。"
  ; #"Your browser is severely outdated and is not supported by this forum anymore. Please get a modern browser.";
$lng->{errUAFeatSup} =
  "あなたのブラウザはこの機能をサポートしていません。";    #"Your browser doesn't support this feature.";
$lng->{errNoCookies} = "ブラウザクッキーが無効になっているので、ログインが機能しません。"
  ;    #"Login won't work because browser cookies are disabled.";
$lng->{errSearchLnk} =
  "リンクされた検索結果は無効になっています。";    #"Linked search results are disabled."; # XXX ?
$lng->{errPlcRead} = "テキストを読んで理解したとされなければなりません。"
  ;    #"The text needs to be marked as read and understood.";

#------------------------------------------------------------------------------
# Help

$lng->{help} = "

<p>注意: tyForumソフトウェアは高度に設定可能なので、このインストール状態で
以下に述べられた機能のすべてが必ずしも有効にされているわけではありません。</p>

<h3>フォーラム</h3>

<p>フォーラムはインストールされたもの全体を指し、通常複数の掲示板を含みます。
あなたはいつも\"forum.pl\"で終わるリンクを通してフォーラムに入るべきです
(\"forum_show.pl\"ではなくて)。
それはいつ新しいセッションを開始するかをフォーラムに知らせるためです。
そうでないと投稿が新しいか古いかをどう表示するかを
フォーラムが知ることができません。</p>

<h3>ユーザ</h3>

<p>ユーザはフォーラムのアカウントを登録する誰でものことです。
通常ほとんどの掲示板を読むのに登録は必要ありませんが、
設定によっては登録されたユーザだけが特定の掲示板や機能を利用できます。</p>

<h3>グループ</h3>

<p>ユーザはユーザグループの中のメンバ権を与えられることがあります。
オープングループはユーザ自身で参加することもできます。
グループは特定の掲示板でメンバやモデレータの権利を与えられ、
グループのメンバはそれらの掲示板を読み書きしたりモデレートすることができます。
</p>

<h3>掲示板</h3>

<p>掲示板はトピックを含み、トピックは投稿を含みます。
掲示板は登録されたユーザに見えるように設定したり
モデレータと掲示板メンバにのみ見えるように設定したりできます。
掲示板は登録していないゲストに投稿を許すこともできます。
アナウンス用掲示板は、モデレータとメンバの投稿のみ許すよう、閲覧のみにすることもでき、
または返信のみ、つまりモデレータとメンバだけが新しいトピックを開始でき、
誰もが返信できるものにすることもできます。
掲示板の他のオプションとしてモデレーションがあります。
このオプションが有効にされると、新しい投稿はモデレータが承認するまで
通常のユーザには見えません。</p>

<h3>トピック</h3>

<p>トピックは、あるいはスレッドとしても知られますが、
特定の主題のすべての投稿を含みます。
題名はトピックの主題に基いて名付けられるべきものです。
掲示板には何日後にトピックが期限切れになるかとか、
最後の投稿がされた何日後に凍結されるかという有効期限があります。
モデレータは、それ以上返信ができないよう、また投稿が編集できないよう、
手動でトピックを凍結することもできます。</p>

<h3>投稿</h3>

<p>投稿はユーザによる公開メッセージです。
それは新しいトピックを開始する土台投稿か、
存在するトピックへの返信かのいずれかです。
投稿は、ある期間に限ってですが、編集したり削除したりできます。
投稿はモデレータによって返信や編集ができないよう凍結されることがあります。
ルール違反の場合、投稿はモデレータに報告されることがあります。</p>

<h3>プライベートメッセージ</h3>

<p>多かれ少なかれ公開である投稿に加えて、
プライベートメッセージがフォーラムで有効にされる場合があります。
登録ユーザは受取人のメールアドレスを知ることなしに
互いにこれらのメッセージを送ることができます。</p>

<h3>管理者</h3>

<p>管理者はフォーラムの中のすべてを制御および編集できます。
これは管理者は全体に対するモデレータでもあることを意味します。
管理者はフォーラム情報ページにリストされます。</p>

<h3>モデレータ</h3>

<p>モデレータの力は特定の掲示板に限定されます。
モデレータは投稿を編集、凍結、削除および承認でき、
トピックを凍結および削除でき、
ルール違反を報告された投稿のリストをチェックできます。
メンバにモデレータの権利があるユーザグループは
掲示板の情報ページにリストされます。</p>

<h3>投票</h3>

<p>トピックの作成者はそのトピックに投票を追加することができます。
それぞれの投票は20個までの選択肢を含むことができます。
投票あたり登録されたユーザあたり1票が許されるか、
または異なる時点での異なる選択肢への複数票が許されます。
投票は編集できず、まだ票が投じられてない場合だけ削除できます。</p>

<h3>アイコン</h3>

<table>
<tr><td>
<img class=\"sic sic_post_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
<img class=\"sic sic_topic_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
<img class=\"sic sic_board_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
</td><td>
黄色のアイコンは新しい投稿かトピック、および新しい投稿のある掲示板を示します。
このフォーラムでは、\"新しい\"はあなたが最後に訪れてから追加された投稿を意味します。
ちょうど今読んだばかりだとしても、それはまだ新しい投稿で、このフォーラムに次に訪れたとき始めて古いものとみなされます。
</td></tr>
<tr><td>
<img class=\"sic sic_post_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
<img class=\"sic sic_topic_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
<img class=\"sic sic_board_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
</td><td>
チェック印のアイコンは投稿やトピックのすべての投稿や掲示板が読まれたことを示します。
投稿は一度そのトピックが画面に現われたり、設定された日数より古いとき既読と数えられます。
このフォーラムでは 新/古 と 未読/既読 は独立した概念なので、
投稿は [古 と 未読] と同様に [新 と 既読] が同時に成り立ちます。
</td></tr>
<tr><td>
<img class=\"sic sic_post_i\" src=\"[[dataPath]]/epx.png\" alt=\"I\">
</td><td>
モデレータによる承認を待っているため
他のユーザに見えない投稿やトピックを示します。
</td></tr>
<tr><td>
<img class=\"sic sic_topic_l\" src=\"[[dataPath]]/epx.png\" alt=\"L\">
</td><td>
凍結された投稿やトピックを示します。
返信や編集はもうできません。
</td></tr>
</table>

<h3>マークアップタグ</h3>

<p>セキュリティ上の理由で、tyForumは独自のマークアップタグのみサポートし、
HTMLタグはサポートしません。利用可能なマークアップタグ:</p>

<table>
<tr><td>[b]text[/b]</td>
<td>テキストを<b>太字</b>で表現する</td></tr>
<tr><td>[i]text[/i]</td>
<td>テキストを<i>斜体</i>で表現する</td></tr>
<tr><td>[tt]text[/tt]</td>
<td>テキストを<code>等幅</code>で表現する</td></tr>
<tr><td>[url]address[/url]</td>
<td>アドレスにリンクする</td></tr>
<tr><td>[url=address]text[/url]</td>
<td>与えられたテキストでアドレスにリンクする</td></tr>
<tr><td>[img]address[/img]</td>
<td>リモートの画像を埋め込む(有効にされていれば)</td></tr>
<tr><td>[img]filename[/img]</td>
<td>添付画像を埋め込む</td></tr>
<tr><td>[img thb]filename[/img]</td>
<td>添付画像のサムネイルを埋め込む(利用可能であれば)</td></tr>
</table>

<h3>引用</h3>

<p>tyForumはメールスタイルの引用を使います。
誰かを引用するには、単純にオリジナルの投稿から1行のテキストを
コピーアンドペーストして &gt; 印を先頭に付けます。
そうすると異なる色でハイライトされます。
文脈を確立するため、必要より多くのテキストを引用しないでください。
フォーラムによっては自動引用が有効にされていますが、
その場合も引用テキストを最小限に削ってください。</p>

<h3>キーボードナビゲーション</h3>

<p>スレッド型掲示板のトピックページの投稿はWASDキーで、
典型的な木構造ビューがカーソルキーでナビゲートできるのと同じ方法で、
ナビゲートできます。
それに加えて、Eキーで次の未読の投稿に飛べます。</p>

";

#"
#
#<p>Note: as the tyForum software is highly configurable, not all of the features
#mentioned below are necessarily enabled in this installation.</p>
#
#<h3>Forum</h3>
#
#<p>The forum is the whole installation, and usually contains multiple boards.
#You should always enter the forum through a link that ends in \"forum.pl\" (not
#\"forum_show.pl\") to let the forum know when you start a new session. Otherwise
#the forum won't know when to display posts as new or old.</p>
#
#<h3>User</h3>
#
#<p>A user is anyone who registers an account in the forum. Registration is
#usually not required for reading most boards, but depending on configuration
#only registered users will have access to certain boards and features.</p>
#
#<h3>Group</h3>
#
#<p>Users can be granted membership in user groups. Open groups can also be
#joined by users themselves. The groups in turn are granted member or moderator
#rights in selected boards, allowing members of the group to read and write in or
#moderate those boards.</p>
#
#<h3>Board</h3>
#
#<p>A board contains topics, which in turn contain the posts. Boards can be set
#to be visible to registered users or to moderators and board members only.
#Boards can optionally allow posts by unregistered guests. Announcement boards
#can be read-only, so that they only allow posts by moderators and members, or
#reply-only, which means that only moderators and members can start new topics,
#but everybody can reply. Another option for boards is moderation. If this option
#is activated, new posts will be invisible to normal users until a moderator
#approves them.</p>
#
#<h3>Topic</h3>
#
#<p>A topic, otherwise known as thread, contains all the posts on a specific
#subtopic, that should be named in the topic's subject. Boards have expiration
#values that determine after how many days their topics will expire or get locked
#after their last post has been made. Moderators can also manually lock topics,
#so that no replies can be made and no post can be edited anymore.</p>
#
#<h3>Post</h3>
#
#<p>A post is a public message by a user. It can be either a base post, which
#starts a new topic, or a reply to an existing topic. Posts can be edited and
#deleted, which may be limited to a certain time frame. Posts can be locked by
#moderators, making it impossible to reply or edit. Posts can be reported to the
#moderators in case of rule violations.</p>
#
#<h3>Private Message</h3>
#
#<p>In addition to the more or less public posts, private messages may be enabled
#in a forum. Registered users can send each other these messages without knowing
#the email addresses of the recipients.</p>
#
#<h3>Administrator</h3>
#
#<p>Administrators can control and edit everything in the forum. This means they
#can also act as moderators globally. Administrators are listed on the forum info
#page.</p>
#
#<h3>Moderator</h3>
#
#<p>Moderators' powers are limited to specific boards. They can edit, lock,
#delete and approve posts, lock and delete topics, and check the list of posts
#reported for rule violations. The user groups whose members have moderator
#rights in a board are listed on the board's info page.</p>
#
#<h3>Polls</h3>
#
#<p>The creator of a topic may be able to add a poll to that topic. Each poll can
#contain up to 20 options. Polls can allow one vote per registered users per
#poll, or alternatively multiple votes for different options at different points
#in time. Polls can't be edited, and can only be deleted as long as there haven't
#been any votes.</p>
#
#<h3>Icons</h3>
#
#<table>
#<tr><td>
#<img class=\"sic sic_post_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
#<img class=\"sic sic_topic_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
#<img class=\"sic sic_board_nu\" src=\"[[dataPath]]/epx.png\" alt=\"N/U\">
#</td><td>
#Yellow icons indicate new posts or topics and boards with new posts.
#In this forum, \"new\" means a post has been added since your last visit. Even
#if you have just read it, it is still a new post, and will only be counted as
#old on your next visit to the forum.
#</td></tr>
#<tr><td>
#<img class=\"sic sic_post_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
#<img class=\"sic sic_topic_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
#<img class=\"sic sic_board_or\" src=\"[[dataPath]]/epx.png\" alt=\"O/R\">
#</td><td>
#Checkmarked icons indicate that the post or all posts in a topic or
#board have been read. Posts are counted as read once their topic had been on
#screen or if they're older than a set number of days. Since new/old and
#unread/read are independent concepts in this forum, posts can be new and read
#as well as old and unread at the same time.
#</td></tr>
#<tr><td>
#<img class=\"sic sic_post_i\" src=\"[[dataPath]]/epx.png\" alt=\"I\">
#</td><td>
#Indicates posts or topics that are invisible to other users, because they
#are waiting for approval by a moderator.
#</td></tr>
#<tr><td>
#<img class=\"sic sic_topic_l\" src=\"[[dataPath]]/epx.png\" alt=\"L\">
#</td><td>
#Indicates posts or topics that have been locked. No replies or edits are
#possible anymore.
#</td></tr>
#</table>
#
#<h3>Markup Tags</h3>
#
#<p>For security reasons, tyForum only supports its own set of markup tags, no
#HTML tags. Available markup tags:</p>
#
#<table>
#<tr><td>[b]text[/b]</td>
#<td>renders text <b>bold</b></td></tr>
#<tr><td>[i]text[/i]</td>
#<td>renders text <i>italic</i></td></tr>
#<tr><td>[tt]text[/tt]</td>
#<td>renders text <code>nonproportional</code></td></tr>
#<tr><td>[url]address[/url]</td>
#<td>links to the address</td></tr>
#<tr><td>[url=address]text[/url]</td>
#<td>links to the address with the given text</td></tr>
#<tr><td>[img]address[/img]</td>
#<td>embeds a remote image (if enabled)</td></tr>
#<tr><td>[img]filename[/img]</td>
#<td>embeds an attached image</td></tr>
#<tr><td>[img thb]filename[/img]</td>
#<td>embeds an attached image's thumbnail (if available)</td></tr>
#</table>
#
#<h3>Quoting</h3>
#
#<p>tyForum uses email-style quoting. To quote someone, simply copy&amp;paste a
#line of text from the original post and prefix it with a &gt; sign. It will then
#get highlighted in a different color. Please don't quote more text than
#necessary to establish context. Some forums may also have automatic quoting
#enabled, in that case please also trim down the quoted text to a minimum.</p>
#
#<h3>Keyboard Navigation</h3>
#
#<p>Posts on topic pages of threaded boards can be navigated with the WASD keys
#in the same way as typical tree view controls can be navigated with the cursor
#keys. In addition, the E key jumps to the next new or unread post.</p>
#
#";

#------------------------------------------------------------------------------
# FAQ

$lng->{faq} = "

<h3>なぜ古い投稿が古いと表示されないのですか？</h3>

<p>あなたは新しいセッションを開始しようとしていることを
フォーラムに知らせるために\"forum.pl\"で終わるリンクを通して
フォーラムに入らなければなりません(\"forum_show.pl\"ではなくて)。
何かの理由で投稿を[古い]にせず古いセッションを続けたいなら、
直接\"forum_show.pl\"を通して入ることができます。</p>

<h3>パスワードをなくしました、パスワードを送ってもらえませんか？</h3>

<p>セキュリティ上の理由で元のパスワードはどこにも保存されていません。
しかしログインページで制限時間内だけ有効なチケットリンク付きのメールを
要求することができます。
そのリンクでログインした後、新しいパスワードを設定することができます。</p>

<h3>セッションの後にログアウトしなければなりませんか？</h3>

<p>あなたは他の信頼できない人も使うコンピュータを使っている場合だけ
ログアウトする必要があります。
tyForumはあなたのユーザIDとパスワードをそのコンピュータのクッキーとして
保存し、それらはログアウトで除去されます。</p>

<h3>どうやって画像やその他のファイルを投稿に添付できますか？</h3>

<p>フォーラムや投稿しようとしている特定の掲示板で添付が有効にされていれば、
まず添付なしで投稿を送信し、その後その投稿の添付ボタンをクリックして
アップロードページに行けます。
投稿とアップロードがこのように分けられているのは、
アップロードは様々な理由で失敗する可能性があり、
そのとき同時に投稿テキストを失わないようにするためです。</p>

";

#"
#
#<h3>Why don't old posts get displayed as old?</h3>
#
#<p>You have to enter the forum through a link that ends in \"forum.pl\" (not
#\"forum_show.pl\") to let the forum know when you want to start a new session.
#Should you for whatever reason want to continue an old session without having
#posts marked as old, you can enter directly through \"forum_show.pl\".</p>
#
#<h3>I lost my password, can you send it to me?</h3>
#
#<p>The original password isn't stored anywhere for security reasons. But on the
#login page you can request an email with a ticket link that is valid for a
#limited time. After using that link to login, you can set a new password.</p>
#
#<h3>Do I have to logout after a session?</h3>
#
#<p>You only need to logout if you are using a computer that is also used by
#other non-trusted persons. tyForum stores your user ID and password via
#cookies on your computer, and these are removed on logout.</p>
#
#<h3>How do I attach images and other files to posts?</h3>
#
#<p>If attachments are enabled in the forum and the specific board you want to
#post in, first submit your post without the attachment, after that you can
#click the post's Attach button to go to the upload page. Posting and uploading
#is separated this way because uploads can fail for various reasons, and you
#probably don't want to lose your post text when that happens.</p>
#
#";

#------------------------------------------------------------------------------

# Load local string overrides
do 'TyfJapaneseLocal.pm';

#------------------------------------------------------------------------------
# Return OK
1;
