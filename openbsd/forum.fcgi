#!/usr/bin/perl

#
# GPLv3+
# Aki Goto <aki@kareha.org>
# based on public/FAQ.html#fcgi by Markus Wichitill
#

#use strict;

#
# Configuration
#

BEGIN {    # for use lib

    package main;

    our $SCRIPT_DIR = '/home/forum/tyforum/script';

}

#goto SKIP;

#
# Daemonize
#

package main;

use Proc::Daemon;
if (Proc::Daemon::Init) {
    exit;
}
$SIG{TERM} = sub { exit; };

END() { }
BEGIN() { }
*CORE::GLOBAL::exit = sub { die "fakeexit\nrc=" . shift() . "\n"; };
eval q{exit};
if ($@) { exit unless $@ =~ /^fakeexit/; }

#SKIP:

#
# Path
#

package main;

use lib $SCRIPT_DIR;
chdir $SCRIPT_DIR;

#
# Configuration for OpenBSD httpd FastCGI
#

package TyfMain;

our $FCGI             = 1;
our $USE_DOCUMENT_URI = 1;

#
# Main loop
#

package main;

use TyfConfigGlobal qw(gcfg);
my $ext    = $TyfConfigGlobal::gcfg->{ext};
my $re_ext = $ext;
$re_ext =~ s/\./\\\./g;

use FCGI;
use Persistent;

my $socket = FCGI::OpenSocket( ":9090", 50 );
my $fcgi   = FCGI::Request( \*STDIN, \*STDOUT, \*STDERR, \%ENV, $socket );
my $p      = Persistent->new();

while ( $fcgi->Accept() >= 0 ) {
    my $request_uri = $ENV{REQUEST_URI};
    $request_uri =~ m/^\/([a-z_]+$re_ext)\??.*/;
    my $script_name = $1;
    if ( $script_name eq "" ) {
        $script_name = "forum$ext";
    }
    my $package = $p->valid_package_name($script_name);
    my $mtime;
    if ( $p->cached( $script_name, $package, \$mtime ) ) {
        eval { $package->handler() };
    }
    else {
        eval { $p->eval_file($script_name) };
    }
}
